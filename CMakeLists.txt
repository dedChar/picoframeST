cmake_minimum_required(VERSION 3.16)

set(CMAKE_TOOLCHAIN_FILE ${CMAKE_CURRENT_SOURCE_DIR}/cmake/stm32_gcc.cmake)
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

project(picoframeST C ASM)
project(bootloader C ASM)

stm32_fetch_cube(L0)

# global options for all targets
add_compile_options(-fdiagnostics-color=always)
# display usage of all memory sections
add_link_options(
    -Wl,--print-memory-usage
)

# find CMSIS files
find_package(CMSIS COMPONENTS STM32L073RB REQUIRED)
# find required HAL drivers
find_package(HAL COMPONENTS STM32L0 CORTEX RCC GPIO CRC FLASH FLASH_RAMFUNC DMA RNG SPI RTC USART UART PCD PWR USB REQUIRED)
set(CMAKE_INCLUDE_CURRENT_DIR TRUE)

list(APPEND OWN_SOURCES
    src/bitmap.c
    src/crc7.c
    src/crc16.c
    src/eeprom.c
    src/eink.c
    src/frame.c
    src/main.c
    src/sdspi.c
    src/syscalls.c
    src/usb_descriptors.c
    src/webusb.c
)
file(GLOB_RECURSE CORE_SOURCES "lib/Core/Src/*.c")

# generate version from git tag
include(cmake/version.cmake)
generate_version_file(VERSION_FILE src/version.c.in)

# collect project source files
set(PROJECT_SOURCES
    ${CORE_SOURCES}
    ${OWN_SOURCES}
    ${VERSION_FILE}
)

set(PROJECT_INCLUDES
    inc
    lib/Core/Inc
)

# add main build target
add_executable(picoframeST ${PROJECT_SOURCES})
target_include_directories(picoframeST PUBLIC ${PROJECT_INCLUDES})

# bootloader build target
add_executable(bootloader src/boot.c)
target_include_directories(bootloader PUBLIC ${PROJECT_INCLUDES})

# build bootloader first and always
add_dependencies(picoframeST bootloader)

# generate additional files
stm32_generate_binary_file(picoframeST)
stm32_generate_hex_file(picoframeST)
stm32_generate_binary_file(bootloader)
stm32_generate_hex_file(bootloader)

# include tinyusb
set(TINYUSB_STM32_FAMILY L0)
include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/tinyusb.cmake)

# link STM libs
target_link_libraries(picoframeST PRIVATE
    HAL::STM32::L0::CORTEX
    HAL::STM32::L0::RCC
    HAL::STM32::L0::RCCEx
    HAL::STM32::L0::GPIO
    HAL::STM32::L0::CRC
    HAL::STM32::L0::CRCEx
    HAL::STM32::L0::DMA
    HAL::STM32::L0::RNG
    HAL::STM32::L0::SPI
    HAL::STM32::L0::RTC
    HAL::STM32::L0::RTCEx
    HAL::STM32::L0::USART
    HAL::STM32::L0::UART
    HAL::STM32::L0::PCD
    HAL::STM32::L0::PCDEx
    HAL::STM32::L0::PWR
    HAL::STM32::L0::PWREx
    HAL::STM32::L0::FLASH
    HAL::STM32::L0::FLASHEx
    HAL::STM32::L0::FLASH_RAMFUNC
    HAL::STM32::L0::LL_USB
    CMSIS::STM32::L073xx  # use L073xx to not use included linker script
    STM32::NoSys
    STM32::Nano
    TinyUSB::Device
)

target_link_libraries(bootloader PRIVATE
    HAL::STM32::L0::RCC
    CMSIS::STM32::L073xx  # use L073xx to not use included linker script
    STM32::NoSys
    STM32::Nano
)

# set optimization flags depending on build type
target_compile_options(picoframeST PUBLIC
    $<$<CONFIG:Debug>:-O1 -g -ggdb>
    $<$<CONFIG:Release>:-Os -s>
    -fdata-sections
    -ffunction-sections
    -flto
    -Wl,--gc-sections
    -includelib/Core/Inc/stm32l0xx_hal_conf.h  # Workaround for missing DMA structs in HAL headers
)

target_link_options(picoframeST PUBLIC
    -Wl,-Map=picoframeST.map
    -Wl,--undefined=_write  # needed to correctly link for Debug builds all the time??
)

target_compile_options(bootloader PUBLIC
    -Os -s
)

target_compile_definitions(picoframeST PUBLIC
    "__weak=__attribute__((weak))"
    "__packed=__attribute__((__packed__))"
    "__eeprom=__attribute__((section(\".eeprom\")))"
    #USE_HAL_DRIVER
    STM32L073xx
    $<$<CONFIG:Release>:STUB_PRINTF>
)

# use custom linker script
stm32_add_linker_script(picoframeST PUBLIC "STM32L073RBTx_main.ld")
stm32_add_linker_script(bootloader PUBLIC "STM32L073RBTx_bootloader.ld")

stm32_print_size_of_target(picoframeST)
stm32_print_size_of_target(bootloader)

# make a combined binary of bootloader and main code
# find python
find_package(Python3)
if(Python3_FOUND)
    message("Python3 found: ${Python3_EXECUTABLE}")
else()
    message(FATAL_ERROR "Python3 not found")
endif()

add_custom_target(combined ALL
    ${Python3_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/scripts/pad.py ${CMAKE_CURRENT_BINARY_DIR}/bootloader.bin ${CMAKE_CURRENT_BINARY_DIR}/bootloader-padded.bin 1024
    COMMAND cat ${CMAKE_CURRENT_BINARY_DIR}/bootloader-padded.bin ${CMAKE_CURRENT_BINARY_DIR}/picoframeST.bin > ${CMAKE_CURRENT_BINARY_DIR}/combined.bin
    DEPENDS bootloader picoframeST
)