cmake_minimum_required(VERSION 3.16)

function(generate_version_file GENERATED_FILE TEMPLATE_FILE)
    # find git
    find_package(Git)
    if(Git_FOUND)
        message("Git found: ${GIT_EXECUTABLE}")
    else()
        message(FATAL_ERROR "Git not found")
    endif()

    # get name of file only
    #get_filename_component(TEMPLATE_NAME ${TEMPLATE_FILE} NAME_WLE)
    set(TEMPLATE_NAME "version")

    # get commit sha with dirty flag
    execute_process(COMMAND
        "${GIT_EXECUTABLE}" describe --match=NeVeRmAtCh --always --abbrev=8 --dirty
        WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
        OUTPUT_VARIABLE GIT_SHA1
        ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    # version from tag
    execute_process(COMMAND
        "${GIT_EXECUTABLE}" describe --tags --always --abbrev=0 --exclude=*[^/]v* --dirty
        WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
        OUTPUT_VARIABLE VERSION_STRING
        ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    # get a comparison tag version to determine if version is wip
    execute_process(COMMAND
        "${GIT_EXECUTABLE}" describe --tags --always --abbrev=0 --exclude=*[^/]v* --exact-match
        WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
        OUTPUT_VARIABLE VERSION_STRING_EXACT
        ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    string(COMPARE NOTEQUAL "${VERSION_STRING_EXACT}" "${VERSION_STRING}" VERSION_IS_DEVEL)

    # try to get just the numbers
    string(REGEX REPLACE
        ".*v([0-9]+\.[0-9]+\.?[0-9]*)(-dirty)?$"
        "\\1" VERSION_STRING_NUMBERS "${VERSION_STRING}"
    )

    # if no tag exists, set version to v0.0.0
    if(VERSION_STRING_NUMBERS STREQUAL VERSION_STRING)
        set(VERSION_STRING_NUMBERS "")
    endif()

    # turn the string into a list
    string(REPLACE "." ";" VERSION_STRING_NUMBERS "${VERSION_STRING_NUMBERS}")

    # add missing numbers
    list(LENGTH VERSION_STRING_NUMBERS NUM_NUMBERS)
    math(EXPR MISSING_NUMBERS "3 - ${NUM_NUMBERS} - 1")
    foreach(i RANGE ${MISSING_NUMBERS})
        list(APPEND VERSION_STRING_NUMBERS 0)
    endforeach()

    # populate version variables
    list(GET VERSION_STRING_NUMBERS 0 VERSION_MAJOR)
    list(GET VERSION_STRING_NUMBERS 1 VERSION_MINOR)
    list(GET VERSION_STRING_NUMBERS 2 VERSION_PATCH)

    message("Setting firmware version: ${VERSION_STRING} (${VERSION_STRING_NUMBERS})")
    message("Setting commit sha: ${GIT_SHA1}")

    # set generated file path
    set(${GENERATED_FILE} "${CMAKE_BINARY_DIR}/generated/${TEMPLATE_NAME}.c" PARENT_SCOPE)

    # generate output file from template
    configure_file(${TEMPLATE_FILE} "${CMAKE_BINARY_DIR}/generated/${TEMPLATE_NAME}.c" @ONLY)

endfunction()