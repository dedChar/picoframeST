cmake_minimum_required(VERSION 3.16)

## CMake script to build tinyusb for ST, cobbled together from the rp2040 tinyusb CMake script

if("${TINYUSB_STM32_FAMILY}" STREQUAL "")
	message(FATAL_ERROR "TINYUSB_STM32_FAMILY not set!")
endif()

# TOP is absolute path to root directory of TinyUSB git repo
set(TOP "${CMAKE_CURRENT_SOURCE_DIR}/lib/tinyusb")
get_filename_component(TOP "${TOP}" REALPATH)

# Base config for both device and host; wrapped by SDK's tinyusb_common
add_library(TinyUSB::Common::Base INTERFACE IMPORTED)

target_sources(TinyUSB::Common::Base INTERFACE
		${TOP}/src/tusb.c
		${TOP}/src/common/tusb_fifo.c
		)

target_include_directories(TinyUSB::Common::Base INTERFACE
		${TOP}/src
		${TOP}/src/common
		${TOP}/hw
		)

target_link_libraries(TinyUSB::Common::Base INTERFACE
        CMSIS::STM32::${TINYUSB_STM32_FAMILY}
		)

# set debug level if not set
if ("${TINYUSB_DEBUG_LEVEL}" STREQUAL "" )
	set(TINYUSB_DEBUG_LEVEL 0)
	if (CMAKE_BUILD_TYPE STREQUAL "Debug")
		message("Compiling TinyUSB with CFG_TUSB_DEBUG=1")
		set(TINYUSB_DEBUG_LEVEL 1)
	endif()
endif()


target_compile_definitions(TinyUSB::Common::Base INTERFACE
		CFG_TUSB_MCU=OPT_MCU_STM32${TINYUSB_STM32_FAMILY}
		CFG_TUSB_DEBUG=${TINYUSB_DEBUG_LEVEL}
)

# Base config for device mode; wrapped by SDK's tinyusb_device
add_library(TinyUSB::Device::Base INTERFACE IMPORTED)
target_sources(TinyUSB::Device::Base INTERFACE
		${TOP}/src/portable/st/stm32_fsdev/dcd_stm32_fsdev.c
        ${TOP}/src/device/usbd.c
		${TOP}/src/device/usbd_control.c
		${TOP}/src/class/audio/audio_device.c
		${TOP}/src/class/cdc/cdc_device.c
		${TOP}/src/class/dfu/dfu_device.c
		${TOP}/src/class/dfu/dfu_rt_device.c
		${TOP}/src/class/hid/hid_device.c
		${TOP}/src/class/midi/midi_device.c
		${TOP}/src/class/msc/msc_device.c
		${TOP}/src/class/net/ecm_rndis_device.c
		${TOP}/src/class/net/ncm_device.c
		${TOP}/src/class/usbtmc/usbtmc_device.c
		${TOP}/src/class/vendor/vendor_device.c
		${TOP}/src/class/video/video_device.c
		)

# create a device library endpoint that combines the required libs
add_library(TinyUSB::Device INTERFACE IMPORTED)
target_link_libraries(TinyUSB::Device INTERFACE
    TinyUSB::Common::Base
    TinyUSB::Device::Base
)

## Host configs not supported on ST devices
# Base config for host mode; wrapped by SDK's tinyusb_host
#add_library(tinyusb_host_base INTERFACE)
#target_sources(tinyusb_host_base INTERFACE
#		${TOP}/src/portable/raspberrypi/rp2040/hcd_rp2040.c
#		${TOP}/src/portable/raspberrypi/rp2040/rp2040_usb.c
#		${TOP}/src/host/usbh.c
#		${TOP}/src/host/usbh_control.c
#		${TOP}/src/host/hub.c
#		${TOP}/src/class/cdc/cdc_host.c
#		${TOP}/src/class/hid/hid_host.c
#		${TOP}/src/class/msc/msc_host.c
#		${TOP}/src/class/vendor/vendor_host.c
#		)

# Sometimes have to do host specific actions in mostly
# common functions
#target_compile_definitions(tinyusb_host_base INTERFACE
#		RP2040_USB_HOST_MODE=1
#)

# This method must be called from the project scope to suppress known warnings in TinyUSB source files
function(suppress_tinyusb_warnings)
	set_source_files_properties(
			${TOP}/src/tusb.c
			PROPERTIES
			COMPILE_FLAGS "-Wno-conversion")
	set_source_files_properties(
			${TOP}/src/common/tusb_fifo.c
			PROPERTIES
			COMPILE_FLAGS "-Wno-conversion -Wno-cast-qual")
	set_source_files_properties(
			${TOP}/src/device/usbd.c
			PROPERTIES
			COMPILE_FLAGS "-Wno-conversion -Wno-cast-qual -Wno-null-dereference")
	set_source_files_properties(
			${TOP}/src/device/usbd_control.c
			PROPERTIES
			COMPILE_FLAGS "-Wno-conversion")
	set_source_files_properties(
			${TOP}/src/class/cdc/cdc_device.c
			PROPERTIES
			COMPILE_FLAGS "-Wno-conversion")
endfunction()
