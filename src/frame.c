#include "frame.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stm32l073xx.h>
#include <stm32l0xx.h>
#include <stm32l0xx_hal.h>
#include <stm32l0xx_hal_crc.h>
#include <stm32l0xx_hal_gpio.h>
#include <stm32l0xx_hal_pwr.h>
#include <stm32l0xx_hal_rcc.h>
#include <stm32l0xx_hal_rng.h>
#include <stm32l0xx_hal_rtc.h>
#include <stm32l0xx_hal_rtc_ex.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>

#include "bitmap.h"
#include "crc16.h"
#include "debug.h"
#include "eeprom.h"
#include "eink.h"
#include "main.h"
#include "sdspi.h"
#include "rng.h"
#include "rtc.h"
#include "version.h"
#include "webusb.h"

#define FRAME_BASE_EPOCH 1609459200  // [s] base timestamp, 2021-jan-01 00:00:00 UTC

#define FRAME_HOURS_TO_SECONDS(hours) ((hours)*60 * 60)  // macro to convert hours to seconds
#define FRAME_MINUTES_TO_SECONDS(minutes) ((minutes)*60)  // macro to convert minutes to seconds


typedef enum {
    FRAME_IMAGE_NONE = 0,
    FRAME_IMAGE_PICTURE,
    FRAME_IMAGE_SETUP,
    FRAME_IMAGE_NO_IMAGES,
    FRAME_IMAGE_USB,
    FRAME_IMAGE_ERROR,
    FRAME_IMAGE_INVALID
} frame_image_content_t;

typedef enum {
    FRAME_BUTTON_NONE,
    FRAME_BUTTON_NEXT,
    FRAME_BUTTON_PREV,
    FRAME_BUTTON_INVALID
} frame_button_t;


typedef struct {
    uint8_t alarm : 1;
    uint8_t usb : 1;
    frame_button_t btn : 2;
} frame_wakeup_flags_t;


static const RTC_DateTypeDef FRAME_BASE_EPOCH_DATE = {
    .Date = 1,
    .Month = RTC_MONTH_JANUARY,
    .Year = 21,
    .WeekDay = RTC_WEEKDAY_FRIDAY
};

static const RTC_TimeTypeDef FRAME_BASE_EPOCH_TIME = {
    .TimeFormat = RTC_FORMAT_BIN,
    .Hours = 0,
    .Minutes = 0,
    .Seconds = 0,
    .SubSeconds = 0,
    .SecondFraction = 0,
    .DayLightSaving = RTC_DAYLIGHTSAVING_NONE,
    .StoreOperation = RTC_STOREOPERATION_SET
};


// put frame settings at deterministic place in eeprom
static frame_settings_t* const frame_settings = EEPROM_GET_PTR(FRAME_SETTINGS_ADDRESS);
static frame_state_t frame_state = FRAME_STATE_INIT;
static frame_image_content_t* const frame_content = EEPROM_GET_PTR(FRAME_DISPLAY_CONTENT_ADDRESS);
static frame_wakeup_flags_t frame_wakeup_flags = {0};
static sdspi_result_t frame_card_error = SDSPI_OK;
static sdspi_cid_t frame_card_cid = {0};
static bitmap_render_t frame_bitmaps[FRAME_BITMAP_BUFFER_SIZE] = {0};
static uint8_t* const frame_shuffle_table = EEPROM_GET_PTR(FRAME_SHUFFLE_TABLE_ADDRESS);
static uint8_t* const frame_shuffle_extra = EEPROM_GET_PTR(FRAME_SHUFFLE_TABLE2_ADDRESS);
static uint16_t frame_current_image = 0;


static bool frame_load_image(const uint16_t image, const size_t part, eink_buffer_t* buffer);
static void frame_handle_card_error(const sdspi_result_t result);
static void frame_setup_bitmaps(const frame_image_content_t content);
static void frame_init_shuffle_table(const uint8_t max);
static uint16_t frame_get_shuffled(const uint16_t idx, const uint16_t max);
static void frame_display_update_cb();
static void frame_prepare_data_cb(const size_t part, eink_buffer_t* buffer);
static void frame_prepare_bitmap_cb(const size_t part, eink_buffer_t* buffer);
static bool frame_check_settings(const frame_settings_t* settings);


void frame_init() {
    bool first_power = false;
    debug_print("started frame init.\n");

    // add artificial alarm flag if first power on
    if (!__HAL_PWR_GET_FLAG(PWR_FLAG_SB)) {
        debug_print("no standby flag, must be first poweron\n");
        frame_wakeup_flags.alarm = true;
        first_power = true;
    }
    __HAL_PWR_CLEAR_FLAG(PWR_FLAG_SB);

    // get wakeup reason
    if (__HAL_RTC_ALARM_GET_FLAG(&hrtc, FRAME_RTC_ALARM_FLAG)) {
        debug_print("got alarm flag\n");
        frame_wakeup_flags.alarm = true;
        HAL_RTC_DeactivateAlarm(&hrtc, FRAME_RTC_ALARM);
        __HAL_RTC_ALARM_CLEAR_FLAG(&hrtc, FRAME_RTC_ALARM_FLAG);
    }

    // check for wakeup from other sources
    if (__HAL_PWR_GET_FLAG(PWR_FLAG_WU)) {
        debug_print("got wakeup flag\n");
        if (HAL_GPIO_ReadPin(VUSB_SENSE_GPIO_Port, VUSB_SENSE_Pin)) {
            frame_wakeup_flags.usb = true;
        }

        if (HAL_GPIO_ReadPin(BTN2_GPIO_Port, BTN2_Pin)) {
            frame_wakeup_flags.btn = FRAME_BUTTON_PREV;
        } else if (HAL_GPIO_ReadPin(BTN1_GPIO_Port, BTN1_Pin)) {
            frame_wakeup_flags.btn = FRAME_BUTTON_NEXT;
        }

        __HAL_PWR_CLEAR_FLAG(PWR_FLAG_WU);
    }

    // display new picture when NRST was triggered
    if (__HAL_RCC_GET_FLAG(RCC_FLAG_PINRST)) {
        debug_print("got reset flag\n");
        frame_wakeup_flags.alarm = true;
        *frame_content = FRAME_IMAGE_NONE;
        first_power = true;
    }
    __HAL_RCC_CLEAR_RESET_FLAGS();

    // init frame content
    if (*frame_content >= FRAME_IMAGE_INVALID) {
        *frame_content = FRAME_IMAGE_NONE;
    }

    // enable peripherals
    HAL_GPIO_WritePin(PERIPH_EN_GPIO_Port, PERIPH_EN_Pin, FRAME_PERIPH_ON);
    HAL_Delay(2);

    // init rtc
    HAL_RTC_DeactivateAlarm(&hrtc, RTC_ALARM_B);
    // ensure no wakeup timer is setup
    HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);

    debug_print("setup eeprom and rtc.\n");
    // init eink
    eink_init(FRAME_EINK_SPI);
    debug_print("setup eink display.\n");

    // init card
    frame_card_error = sdspi_init(FRAME_CARD_SPI);
    frame_handle_card_error(frame_card_error);

    // get card cid
    frame_card_error = sdspi_get_cid(&frame_card_cid);
    frame_handle_card_error(frame_card_error);


    // end here if card error occured
    if (frame_card_error != SDSPI_OK) {
        frame_state = FRAME_STATE_ERROR;
        return;
    };

    // settings are initialized
    if (!frame_check_settings(frame_settings)) {
        debug_print("settings invalid, resetting settings\n");

        // reset settings if unknown version
        if (frame_settings->fields.version >= FRAME_SETTINGS_UNSUPPORTED ||
            frame_settings->fields.version == FRAME_SETTINGS_NONE) {
            frame_reset_settings(frame_settings);
        }

        frame_state = FRAME_STATE_SETUP;
        return;
    }
    debug_print("eeprom settings all valid.\n");

    // initialize shuffle tables if done settings are valid
    if (first_power) frame_init_shuffle_table(frame_settings->fields.num_images);

    debug_print("frame init completed, displaying first image.\n");

    // put into idle
    frame_state = FRAME_STATE_IDLE;
}

void frame_task() {
    // ensure led is on when awake
    HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, !frame_settings->fields.flags.bits.led_disabled);
    // set usb led if usb is connected
    HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, usb_device_connected());

    switch (frame_state) {
        case FRAME_STATE_INIT:
            // run init
            *frame_content = FRAME_IMAGE_NONE;
            frame_init();
            break;
        case FRAME_STATE_IDLE:
            // go to sleep
            if (!frame_is_usb_powered()) {
                frame_state = FRAME_STATE_GO_SLEEP;
            }

            // go into no image state
            if (frame_settings->fields.num_images == 0) {
                debug_print("no images loaded, entering NO_IMAGES state\n");
                frame_state = FRAME_STATE_NO_IMAGES;
                break;
            }

            // get wakeup reason
            if (frame_wakeup_flags.btn == FRAME_BUTTON_PREV || frame_wakeup_flags.btn == FRAME_BUTTON_NEXT) {
                // set alarm flag to update screen if wakeup was by button press
                frame_wakeup_flags.alarm = true;
            } else {
                frame_wakeup_flags.btn = FRAME_BUTTON_NONE;
            }

            if (frame_wakeup_flags.alarm) {
                if (!eink_is_updating()) {
                    // display next image on alarm
                    debug_print("got alarm flag, entering DISPLAY state\n");
                    frame_state = FRAME_STATE_DISPLAY;
                } else {
                    // got alarm flag, but display is still busy, will need to wait this out
                    frame_state = FRAME_STATE_IDLE;
                }
            }

            if (frame_wakeup_flags.usb) {
                // just clear this flag
                debug_print("got vusb flag\n");
                frame_wakeup_flags.usb = false;
            }

            if (usb_device_connected()) {
                // go into webusb state if pc is connected
                debug_print("usb host connected, entering USB_CONNECTED\n");
                frame_state = FRAME_STATE_WEBUSB_CONNECTED;
            }

            break;
        case FRAME_STATE_SETUP:
            // display setup instructions
            if (*frame_content != FRAME_IMAGE_SETUP && !eink_is_updating()) {
                frame_setup_bitmaps(FRAME_IMAGE_SETUP);
                eink_update(NULL, frame_prepare_bitmap_cb);
                *frame_content = FRAME_IMAGE_SETUP;
            }

            // check settings validiy again
            if (frame_settings->fields.flags.bits.setup_completed && !usb_device_connected()) {
                debug_print("checking new settings without card cid and crc\n");
                // overwrite card cid, don't have to know this on host
                memcpy(frame_settings->fields.card_cid, frame_card_cid, sizeof(frame_card_cid));
                // recalculate crc16
                frame_settings->fields.crc16 = crc16_get(frame_settings->data, sizeof(frame_settings->data));

                // check validity
                if (frame_check_settings(frame_settings)) {
                    debug_print("got valid settings after setup, going to idle\n");
                    // go into idle and display image
                    frame_wakeup_flags.alarm = true;
                    frame_state = FRAME_STATE_IDLE;
                } else {
                    debug_print("got invalid settings after setup, staying in setup\n");
                    // reset setup complete flag
                    frame_settings->fields.flags.bits.setup_completed = false;
                }
                // save settings
                frame_write_settings(frame_settings);
            }

            // wait for setup via webusb
            if (!frame_is_usb_powered() && !eink_is_updating()) {
                frame_enter_sleep(false, true, false);
            }
            break;
        case FRAME_STATE_NO_IMAGES:
            // display instructions
            if (*frame_content != FRAME_IMAGE_NO_IMAGES && !eink_is_updating()) {
                frame_setup_bitmaps(FRAME_IMAGE_NO_IMAGES);
                eink_update(NULL, frame_prepare_bitmap_cb);
                *frame_content = FRAME_IMAGE_NO_IMAGES;
            }


            // check validity
            if (frame_settings->fields.num_images != 0 && !usb_device_connected()) {
                debug_print("detected new number of images\n");
                // change state to idle, if settings are valid
                if (!frame_check_settings(frame_settings)) {
                    debug_print("number of images is out of range, going into setup\n");
                    frame_state = FRAME_STATE_SETUP;
                } else {
                    debug_print("got valid number of images, going idle\n");
                    frame_wakeup_flags.alarm = true;
                    frame_state = FRAME_STATE_IDLE;
                }
            }

            // wait for setup via webusb
            if (!frame_is_usb_powered() && !eink_is_updating()) {
                frame_enter_sleep(false, true, false);
            }
            break;
        case FRAME_STATE_DISPLAY:
            debug_print("displaying new image\n");
            // get next or previous image in sequence, depending on button
            if (frame_wakeup_flags.btn == FRAME_BUTTON_PREV) {
                if (frame_settings->fields.last_image == 0) {
                    frame_settings->fields.last_image = frame_settings->fields.num_images - 1;
                } else {
                    frame_settings->fields.last_image--;
                }
            } else {
                frame_settings->fields.last_image++;
                frame_settings->fields.last_image %= frame_settings->fields.num_images;
            }
            debug_print("selected image #%d for display\n", frame_settings->fields.last_image);

            // save updated settings
            frame_write_settings(frame_settings);

            // get a shuffled index if shuffle is enabled
            frame_current_image = frame_settings->fields.last_image;
            if (frame_settings->fields.flags.bits.shuffle && frame_settings->fields.num_images > 1) {
                frame_current_image = frame_get_shuffled(frame_current_image, frame_settings->fields.num_images);
            }

            // check if image index is valid
            if ((sdspi_card_inserted() && frame_current_image >= frame_max_images()) || frame_current_image >= frame_settings->fields.num_images) {
                *frame_content = FRAME_IMAGE_NONE;
                if (frame_state != FRAME_STATE_ERROR) {
                    // loading image failed because index is out of range --> invalid settings
                    debug_print("image index out of range! entering setup mode!\n");
                    frame_state = FRAME_STATE_SETUP;
                    break;
                }
            }

            *frame_content = FRAME_IMAGE_PICTURE;

            // setup next alarm, if more than one image
            if (frame_settings->fields.num_images > 1) {
                if (frame_wakeup_flags.alarm) {
                    frame_setup_alarm(frame_settings->fields.timer);
                }
            } else {
                HAL_RTC_DeactivateAlarm(&hrtc, FRAME_RTC_ALARM);
                frame_wakeup_flags.alarm = false;
            }

            // update display and leave DISPLAY state once done
            frame_state = FRAME_STATE_WAITING;
            eink_update(frame_display_update_cb, frame_prepare_data_cb);
            break;
        case FRAME_STATE_WAITING:
            // do nothing in waiting state, state change occurs from external source
            break;
        case FRAME_STATE_WEBUSB_CONNECTED:
            // display usb icon when connected
            if (*frame_content != FRAME_IMAGE_USB && !eink_is_updating()) {
                // turn on second led to indicate connection
                *frame_content = FRAME_IMAGE_USB;
            }

            // back to idle on disconnect
            if (!usb_device_connected() && !eink_is_updating()) {
                // turn led back off
                HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, GPIO_PIN_RESET);
                debug_print("webusb connection stopped, refreshing and validating eeprom settings\n");

                // check settings validity
                if (!frame_check_settings(frame_settings)) {
                    debug_print("settings are invalid, going into setup\n");
                    // return to setup if invalid settings
                    frame_state = FRAME_STATE_SETUP;
                } else {
                    debug_print("settings all valid, going idle\n");
                    // setup a new shuffle table
                    frame_init_shuffle_table(frame_settings->fields.num_images);
                    // return to idle with alarm flag set to update image
                    frame_wakeup_flags.alarm = true;
                    frame_state = FRAME_STATE_IDLE;
                }
            }
            break;
        case FRAME_STATE_UPDATE:
            break;
        case FRAME_STATE_GO_SLEEP:
            // enter sleep mode
            if (!frame_is_usb_powered() && !eink_is_updating() && !usb_device_connected()) {
                // allow wakeup from alarm and usb
                // allow wakeup from button press if more than one image
                const bool wake_btn = frame_settings->fields.num_images > 1 && !frame_settings->fields.flags.bits.buttons_disabled;
                frame_enter_sleep(true, true, wake_btn);
            }
            break;
        case FRAME_STATE_ERROR:
            // display error
            if (*frame_content != FRAME_IMAGE_ERROR && !eink_is_updating()) {
                frame_setup_bitmaps(FRAME_IMAGE_ERROR);
                eink_update(NULL, frame_prepare_bitmap_cb);
                *frame_content = FRAME_IMAGE_ERROR;
            }
            // enter sleep
            if (!frame_is_usb_powered() && !eink_is_updating()) {
                // retry init when frame is connected to usb in sleep
                frame_enter_sleep(false, true, false);
            }
            break;
        case FRAME_STATE_NONE:
            break;
    }
}

void frame_get_device_name(frame_device_name_t* dest) {
    static const uint8_t* const uid1 = (void*)UID_BASE;
    static const uint8_t* const uid2 = (void*)UID_BASE + 0x14;
    uint32_t crc = 0;

    // calculate two crc16s
    crc = crc16_get(uid1, 8);
    crc |= crc16_get(uid2, 4) << 16;

    // get from settings
    switch (frame_settings->fields.version) {
        case FRAME_SETTINGS_V1:
            // copy name if set
            if (frame_settings->fields.flags.bits.name_set) {
                memcpy(*dest, frame_settings->fields.device_name, FRAME_DEVICE_NAME_LENGTH);
                return;
            }
            // fall through, if no name set
        case FRAME_SETTINGS_NONE:  // fall through
        default:
            // use crc of pico unique id
            snprintf(*dest, FRAME_DEVICE_NAME_LENGTH, "0x%08lx", crc);
            break;
    }
}

void frame_read_settings(frame_settings_t* settings) {
    // read data from start of eeprom
    memcpy(settings, frame_settings, sizeof(frame_settings_t));
}

void frame_write_settings(frame_settings_t* settings) {
    // ensure timer data is valid
    settings->fields.timer.minutes %= 60;

    // calculate crc16 of data
    settings->fields.crc16 = crc16_get(settings->data, sizeof(settings->data));

    // copy data if called from outside of module
    if (settings != frame_settings) {
        memcpy(frame_settings, settings, sizeof(frame_settings_t));
    }
}

void frame_reset_settings(frame_settings_t* settings) {
    // reset settings to 0
    memset(settings->raw, 0, sizeof(frame_settings_t));

    // set newest supported version
    settings->fields.version = FRAME_SETTINGS_UNSUPPORTED - 1;
    // set card id if available
    sdspi_get_cid(&settings->fields.card_cid);

    // write settings to eeprom
    frame_write_settings(settings);
}

frame_info_t frame_get_info() {
    frame_info_t info = {
        .state = frame_state,
        .is_dev = version_is_dev(),
        .is_bank2 = READ_BIT(SYSCFG->CFGR1, SYSCFG_CFGR1_UFB),
        .max_images = frame_max_images(),
        .card_inserted = sdspi_card_inserted(),
        .card_type = sdspi_get_card(),
        .last_card_error = frame_card_error
    };

    return info;
}

void frame_enter_sleep(const bool wakeup_alarm, const bool wakeup_usb, const bool wakeup_buttons) {
    // put display into sleep
    eink_sleep();

    // disconnect peripherals
    HAL_GPIO_WritePin(PERIPH_EN_GPIO_Port, PERIPH_EN_Pin, FRAME_PERIPH_OFF);

    // get raw timer value to easily check if no time is set
    const uint16_t timer_val = *(uint16_t*)(&frame_settings->fields.timer);
    // setup alarm and/or usb wakeup
    if (wakeup_alarm && timer_val != 0) {
        // setup alarm, if no alarm was setup prior
        if (!READ_BIT(RTC->CR, RTC_CR_ALRAE) || !READ_BIT(RTC->CR, RTC_CR_ALRAIE)) {
            frame_setup_alarm(frame_settings->fields.timer);
        }
    } else {
        HAL_RTC_DeactivateAlarm(&hrtc, FRAME_RTC_ALARM);
    }

    if (wakeup_usb) {
        HAL_PWR_EnableWakeUpPin(FRAME_VUSB_WAKE_PIN);
    } else {
        HAL_PWR_DisableWakeUpPin(FRAME_VUSB_WAKE_PIN);
    }

    if (wakeup_buttons) {
        HAL_PWR_EnableWakeUpPin(FRAME_BTN_WAKE_PIN);
    } else {
        HAL_PWR_DisableWakeUpPin(FRAME_BTN_WAKE_PIN);
    }

    // turn off LED
    HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);

    debug_print("going to sleep!\n");


    // enter standby mode
    HAL_PWR_EnterSTANDBYMode();

    // won't get here
}

void frame_setup_alarm(const frame_timer_t timer) {
    HAL_RTC_DeactivateAlarm(&hrtc, FRAME_RTC_ALARM);

    // get number of days from hours
    const uint8_t days = timer.hours / 24;
    const uint8_t hours = timer.hours % 24;

    RTC_AlarmTypeDef alarm = {
        .Alarm = FRAME_RTC_ALARM,
        .AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE,  // use date for alarm trigger
        .AlarmDateWeekDay = FRAME_BASE_EPOCH_DATE.Date + days,
        .AlarmSubSecondMask = RTC_ALARMSUBSECONDMASK_ALL,  // ignore subseconds
        .AlarmMask = RTC_ALARMMASK_NONE,  // trigger when day, hour and minute match
        .AlarmTime = {0}
    };

    // copy base epoch values
    memcpy(&alarm.AlarmTime, &FRAME_BASE_EPOCH_TIME, sizeof(RTC_TimeTypeDef));
    alarm.AlarmTime.Hours += hours;
    alarm.AlarmTime.Minutes += timer.minutes;

    // set base epoch
    HAL_RTC_SetDate(&hrtc, (RTC_DateTypeDef*)&FRAME_BASE_EPOCH_DATE, RTC_FORMAT_BIN);
    HAL_RTC_SetTime(&hrtc, (RTC_TimeTypeDef*)&FRAME_BASE_EPOCH_TIME, RTC_FORMAT_BIN);
    // set alarm
    HAL_RTC_SetAlarm_IT(&hrtc, &alarm, RTC_FORMAT_BIN);
    frame_wakeup_flags.alarm = false;
}


static bool frame_load_image(const uint16_t image, const size_t part, eink_buffer_t* buffer) {
    // get location of image on card
    const uint32_t block = (image * EINK_IMAGE_BYTES + part * EINK_BUFFER_SIZE) / SDSPI_BLOCK_SIZE;
    const uint16_t offset = (image * EINK_IMAGE_BYTES + part * EINK_BUFFER_SIZE) % SDSPI_BLOCK_SIZE;
    // load image into eink buffer
    frame_card_error = sdspi_read(block, offset, *buffer, EINK_BUFFER_SIZE);
    // handle potential card error
    frame_handle_card_error(frame_card_error);

    return frame_card_error == SDSPI_OK;
}

static void frame_handle_card_error(const sdspi_result_t result) {
    // everything ok
    if (result == SDSPI_OK) return;

    // some error occured
    // setup error screen
    eink_fill_color(EINK_COLOR_WHITE);
    switch (result) {
        case SDSPI_ERROR_UNKNOWN_CARD:
            // display unknown card error
            debug_print("unknown card error!\n");
            break;
        case SDSPI_ERROR_NO_CARD:
            // display no card error
            debug_print("no card error!\n");
            break;
        case SDSPI_ERROR_READ:
        case SDSPI_ERROR_GENERIC:
        case SDSPI_ERROR_RESPONSE_TIMEOUT:
        case SDSPI_ERROR_RESET:
        case SDSPI_ERROR_WRITE:
        case SDSPI_ERROR_INVALID_ARGS:
        default:
            // some operation error occured
            debug_print("different card error occured: %d\n", result);
            frame_state = FRAME_STATE_ERROR;
            break;
    }
}

static void frame_setup_bitmaps(const frame_image_content_t content) {
    // setup qrcode on all
    BITMAP_PREP_STRUCT(frame_bitmaps, QRCODE);
    frame_bitmaps->x = 50;
    frame_bitmaps->y = 30;
    frame_bitmaps->background = EINK_COLOR_WHITE;
    frame_bitmaps->foreground = EINK_COLOR_BLACK;
    frame_bitmaps->scale = 2;

    // use next slot
    bitmap_render_t* bm = &frame_bitmaps[0];
    bm++;

    switch (content) {
        case FRAME_IMAGE_SETUP:
            BITMAP_PREP_STRUCT(bm, SCAN_AND_SETUP);
            bm->x = 38;
            bm->y = 90;
            bm->background = EINK_COLOR_WHITE;
            bm->foreground = EINK_COLOR_BLACK;
            bm->scale = 1;
            break;
        case FRAME_IMAGE_NO_IMAGES:
            BITMAP_PREP_STRUCT(bm, NO_IMAGE);
            bm->x = 48;
            bm->y = 92;
            bm->background = EINK_COLOR_WHITE;
            bm->foreground = EINK_COLOR_RED;
            bm->scale = 1;
            break;
        case FRAME_IMAGE_ERROR:
            BITMAP_PREP_STRUCT(bm, CARD_ERROR);
            bm->x = 45;
            bm->y = 92;
            bm->background = EINK_COLOR_WHITE;
            bm->foreground = EINK_COLOR_RED;
            bm->scale = 1;
            break;
        default:
            // disable all
            for (size_t i = 0; i < FRAME_BITMAP_BUFFER_SIZE; i++) {
                frame_bitmaps[i].bitmap = NULL;
            }
            break;
    }
}

static bool frame_check_settings(const frame_settings_t* settings) {
    if (settings->fields.version == FRAME_SETTINGS_V1) {
        // check settings crc
        if (!crc16_verify(settings->fields.crc16, settings->data, sizeof(settings->data))) {
            // crc failed, put into setup state
            debug_print("settings crc16 did not match\n");
            return false;
        }

        // check card cid
        if (memcmp(frame_card_cid, settings->fields.card_cid, SDSPI_CID_CSD_SIZE) != 0) {
            // card cid did not match, put into setup
            debug_print("saved card cid did not match\n");
            return false;
        }

        // check num_images
        if (settings->fields.num_images >= frame_max_images()) {
            debug_print("number of images is out of bounds\n");
            return false;
        }

        // check setup complete flag
        if (!settings->fields.flags.bits.setup_completed) {
            debug_print("setup not completed yet\n");
            return false;
        }
    } else {
        // unrecognized settings data, put into setup mode
        debug_print("unrecoginzed settings version\n");
        return false;
    }

    // settings passed
    return true;
}

static void frame_init_shuffle_table(const uint8_t max) {
    static uint8_t *const tables[2] = {frame_shuffle_table, frame_shuffle_extra};

    for (size_t i = 0; i < 2; i++) {
        uint8_t *const tbl = tables[i];
        // init used index array with unused value
        memset(tbl, 0xff, FRAME_SHUFFLE_TABLE_MAX_SIZE);

        // get maximum allowed entries
        uint8_t max_entries;
        if (tbl == frame_shuffle_table) {
            max_entries = max > FRAME_SHUFFLE_TABLE_MAX_SIZE ? FRAME_SHUFFLE_TABLE_MAX_SIZE : max;
        } else {
            max_entries = max % FRAME_SHUFFLE_TABLE_MAX_SIZE;
        }
        uint8_t entries_left = max_entries;

        while (entries_left) {
            uint8_t candidate = HAL_RNG_GetRandomNumber(&hrng) % max_entries;
            // check if candidate is a duplicate
            uint8_t* entry = tbl;
            bool duplicate = false;
            while (*entry != 0xff) {
                if (*entry == candidate) {
                    duplicate = true;
                    break;
                }
                entry++;
            }

            // try again if duplicate
            if (duplicate) continue;
            // store entry
            *entry = candidate;
            entries_left--;
        }
    }
}

static uint16_t frame_get_shuffled(const uint16_t idx, const uint16_t max) {
    const uint8_t shuffle_idx = idx % FRAME_SHUFFLE_TABLE_MAX_SIZE;
    const uint16_t offset = idx - shuffle_idx;

    const uint16_t end = max - (max % FRAME_SHUFFLE_TABLE_MAX_SIZE);
    if (idx >= end) {
        // the remainder of images that are left have their own table
        return frame_shuffle_extra[shuffle_idx] + offset;
    } else {
        return frame_shuffle_table[shuffle_idx] + offset;
    }
}

static void frame_display_update_cb() {
    // called in DISPLAY state, after eink display is done updating

    // setup next alarm
    if (frame_wakeup_flags.alarm) {
        frame_setup_alarm(frame_settings->fields.timer);
    }

    // go to idle or sleep depending on usb connection
    if (!frame_is_usb_powered()) {
        frame_state = FRAME_STATE_GO_SLEEP;
    } else {
        frame_state = FRAME_STATE_IDLE;
    }
}

/** @brief  Data preparation callback for eink_update(). Loads a slice of an image from card.
 *  @param  part The current part of the image to load.
 *  @param  buffer Pointer to the image buffer used to send the data to the screen.
 */
static void frame_prepare_data_cb(const size_t part, eink_buffer_t* buffer) {
    // load data from sd
    frame_load_image(frame_current_image, part, buffer);
}

/** @brief  Data preparation callback for eink_update(). Draws the bitmaps in frame_bitmaps array.
 *  @param  part The current part of the image to load.
 *  @param  buffer Pointer to the image buffer used to send the data to the screen.
 *
 *  The bitmaps should be set up in the frame_bitmaps array.
 *  If bitmaps overlap the one with a higher index in the array will be on top.
 */
static void frame_prepare_bitmap_cb(const size_t part, eink_buffer_t* buffer) {
    // lookup table for color bytes
    static const uint8_t COLOR_LUT[EINK_COLOR_CLEAN] = {
        EINK_COLOR_BYTE(EINK_COLOR_BLACK),
        EINK_COLOR_BYTE(EINK_COLOR_WHITE),
        EINK_COLOR_BYTE(EINK_COLOR_GREEN),
        EINK_COLOR_BYTE(EINK_COLOR_BLUE),
        EINK_COLOR_BYTE(EINK_COLOR_RED),
        EINK_COLOR_BYTE(EINK_COLOR_YELLOW),
        EINK_COLOR_BYTE(EINK_COLOR_ORANGE),
    };

    const size_t bytes_line = sizeof(*buffer) / EINK_LINES_PER_PART;
    // fill one line with white
    memset(*buffer, COLOR_LUT[EINK_COLOR_WHITE], bytes_line);

    // draw all bitmaps inside frame_bitmaps buffer
    // highest index will be on top
    for (size_t bm = 0; bm < FRAME_BITMAP_BUFFER_SIZE; bm++) {
        bitmap_render_t* bm_info = &frame_bitmaps[bm];
        if (bm_info->bitmap == NULL) {
            break;
        }
        // current line number of bitmap
        const size_t y = (part - bm_info->y) / bm_info->scale;
        // get foreground and background color
        const uint8_t fcol = COLOR_LUT[bm_info->foreground];
        const uint8_t bcol = COLOR_LUT[bm_info->background];
        // draw to line if current part is where the image will be seen
        if (part >= bm_info->y && y < bm_info->pixel_height) {
            for (size_t x = 0; x < bm_info->pixel_width && (x * FRAME_BITMAP_BASE_SCALE_X) < bytes_line; x += 8) {
                // padding bits included in each row
                const uint8_t padding = (8 - (bm_info->pixel_width % 8)) % 8;
                const uint8_t byte = bm_info->bitmap[(x + y * (bm_info->pixel_width + padding)) / 8];
                // go through each pixel in the byte
                for (size_t pxl = 0; pxl < 8; pxl++) {
                    // abort if end of row was reached
                    if (x + pxl >= bm_info->pixel_width) {
                        break;
                    }

                    // x and width doubled, because 4 pixels are drawn with 2 pixels stored in one byte
                    size_t buffer_idx = bm_info->x * FRAME_BITMAP_BASE_SCALE_X + (x + pxl) * FRAME_BITMAP_BASE_SCALE_X * bm_info->scale;
                    // number of bytes that need to be written
                    size_t num_bytes = 2 * bm_info->scale;
                    // don't overflow out of the line
                    if (buffer_idx + num_bytes > bytes_line) {
                        num_bytes = bytes_line - buffer_idx;
                    }

                    // set color of the scaled pixel
                    memset(*buffer + buffer_idx, ((byte >> pxl) & 1) ? fcol : bcol, num_bytes);
                }
            }
        }
    }

    // duplicate line to other lines
    uint8_t* dest = *buffer + bytes_line;
    while (dest != *buffer + bytes_line * EINK_LINES_PER_PART) {
        memcpy(dest, *buffer, bytes_line);
        dest += bytes_line;
    }
}


void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *hrtc) {
    debug_print("got alarm while on! triggering new image\n");
    frame_wakeup_flags.alarm = true;
    frame_state = FRAME_STATE_IDLE;

    // deactivate alarm and clear flag
    HAL_RTC_DeactivateAlarm(hrtc, FRAME_RTC_ALARM);
    __HAL_RTC_ALARM_CLEAR_FLAG(hrtc, FRAME_RTC_ALARM_FLAG);
    NVIC_ClearPendingIRQ(RTC_IRQn);
}
