#include "eink.h"
#include "main.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stm32l073xx.h>
#include <stm32l0xx_hal.h>
#include <stm32l0xx_hal_cortex.h>
#include <stm32l0xx_hal_exti.h>
#include <stm32l0xx_hal_gpio.h>
#include <stm32l0xx_hal_spi.h>
#include <string.h>


static uint8_t eink_buffer[EINK_BUFFER_SIZE] = {0};
static SPI_HandleTypeDef* eink_spi = NULL;
static eink_update_cb_t eink_busy_cb = NULL;
static eink_update_cb_t eink_update_cb = NULL;
static eink_update_cb_t eink_clean_cb = NULL;
static eink_prep_data_cb_t eink_data_cb = NULL;
static volatile bool eink_busy_flag = false;
static volatile bool eink_busy_state = false;
static bool eink_busy_waiting_state = false;
static bool eink_updating = false;


static void eink_wait_busy(const bool state);
static void eink_wait_busy_interrupt(const bool state, eink_update_cb_t callback);
static void eink_command(const eink_command_t cmd);
static void eink_data(const uint8_t* data, const size_t length);
static void eink_set_tres();
static void eink_clean_finish_cb();
static void eink_update_start_cb();
static void eink_update_finish_cb();


/**
 * @brief   Initialize spi and gpio. Then start reset sequence.
 */
void eink_init(SPI_HandleTypeDef* spi) {
    // set buffer to all white
    memset(eink_buffer, 0x11, EINK_BUFFER_SIZE);

    // setup gpio pin as EXTI interrupt
    GPIO_InitTypeDef init = {0};
    init.Pin = EINK_BUSY_Pin;
    init.Mode = GPIO_MODE_IT_RISING_FALLING;
    init.Pull = GPIO_NOPULL;
    init.Speed = GPIO_SPEED_FREQ_LOW;
    init.Alternate = 0;

    HAL_GPIO_Init(EINK_BUSY_GPIO_Port, &init);

    // enable EXTI interrupt
    __HAL_GPIO_EXTI_CLEAR_IT(EINK_BUSY_Pin);
    HAL_NVIC_ClearPendingIRQ(EXTI0_1_IRQn);
    HAL_NVIC_SetPriority(EXTI0_1_IRQn, 3, 0);
    HAL_NVIC_EnableIRQ(EXTI0_1_IRQn);

    eink_spi = spi;
    HAL_GPIO_WritePin(EINK_RST_GPIO_Port, EINK_RST_Pin, GPIO_PIN_SET);

    // do reset sequence
    eink_reset();
}

/**
 * @brief   Do a reset sequence.
 */
void eink_reset() {
    // trigger hardware reset
    eink_trigger_reset();

    eink_wait_busy(true);
    // setup panel setting
    // sets scanning direction to scan down
    // sets shift right
    // turn on charge pump
    static const uint8_t panel_setting[2] = {0xef, 0x08};
    eink_command(EINK_COMMAND_PANEL_SETTING);
    eink_data(panel_setting, 2);

    // setup power settings
    // set all power sources to internal
    static const uint8_t power_setting[4] = {0x37, 0x00, 0x23, 0x23};
    eink_command(EINK_COMMAND_POWER_SETTING);
    eink_data(power_setting, 4);

    // set power off sequence to 1 frame
    static const uint8_t power_seq = 0x00;
    eink_command(EINK_COMMAND_POWER_OFF_SEQ);
    eink_data(&power_seq, 1);

    // set booster soft start
    static const uint8_t booster_data[3] = {0xc7, 0xc7, 0x1d};
    eink_command(EINK_COMMAND_BOOSTER_SOFT_START);
    eink_data(booster_data, 3);

    // set frame rate to 50Hz
    static const uint8_t framerate_data = 0x3c;
    eink_command(EINK_COMMAND_PLL_CONTROL);
    eink_data(&framerate_data, 1);

    // enable temperature sensor
    eink_command(EINK_COMMAND_TEMP_SENSOR_ENABLE);
    eink_data(&power_seq, 1);  // also just write 0x00

    // setup VCOM interval, color table and border color
    // vcom data interval = 10
    // default color table order (see eink_color_t)
    // blue border
    static const uint8_t vcom_setting = 0x37;
    eink_command(EINK_COMMAND_VCOM_SETTING);
    eink_data(&vcom_setting, 1);

    // set resolutions
    // tcon
    static const uint8_t tcon_setting = 0x22;
    eink_command(EINK_COMMAND_TCON);
    eink_data(&tcon_setting, 1);

    // tres
    static const uint8_t tres_setting[4] = {0x02, 0x58, 0x01, 0xc0};
    eink_command(EINK_COMMAND_TRES);
    eink_data(tres_setting, 4);

    // setup VCOM powersave
    static const uint8_t powersave_setting = 0xaa;
    eink_command(EINK_COMMAND_POWERSAVE);
    eink_data(&powersave_setting, 1);


    HAL_Delay(100);
    // set vcom settings again (?)
    eink_command(EINK_COMMAND_VCOM_SETTING);
    eink_data(&vcom_setting, 1);
}

/**
 * @brief   Change a single pixel in data buffer.
 * @param   x Pixel x coordinate.
 * @param   y Pixel y coordinate.
 * @param   color Pixel color.
 */
void eink_set_pixel(const size_t x, const size_t y, const eink_color_t color) {
    if (x >= EINK_WIDTH || y >= EINK_HEIGHT) return;
    // set the pixel in the correct nibble in buffer
    EINK_BUFFER_SET(eink_buffer, x, y, color);
}

/**
 * @brief   Draw a bitmap at a specified screen location.
 * @param   x Upper-Left x-Coordinate of bitmap on screen.
 * @param   y Upper-Left y-Coordinate of bitmap on screen.
 * @param   w Width of bitmap in pixels.
 * @param   h Height of bitmap in pixels.
 */
void eink_draw_bitmap(const uint8_t* bitmap, const size_t x, const size_t y, const size_t w, const size_t h) {
    if (x >= EINK_WIDTH || y >= EINK_HEIGHT || w == 0 || h == 0) return;

    const bool odd_width = w % 2 == 1;
    const bool odd_start = x % 2 == 1;

    const size_t last_valid_row = EINK_HEIGHT - y;
    const size_t last_valid_col = EINK_WIDTH - x;

    // copy data row by row
    for (uint32_t row = 0; row < h && row < last_valid_row; row++) {
        // need to do this manually because of misalignment
        if (odd_start) {
            for (uint32_t col = 0; col < w && col < last_valid_col; col++) {
                const eink_color_t color = EINK_PIXEL_GET(bitmap, w, col, row);
                eink_set_pixel(x + col, y + row, color);
            }
        } else {
            const size_t max_width = last_valid_col >= (x + w) ? w : last_valid_col;
            // can use memcpy
            memcpy(eink_buffer + EINK_BUFFER_PIXEL_IDX(x, y + row), bitmap + EINK_PIXEL_IDX(w, 0, row), max_width / 2);
            // need to do last pixel extra of odd width
            if (odd_width && max_width == w) {
                const eink_color_t color = EINK_PIXEL_GET(bitmap, w, max_width - 1, row);
                eink_set_pixel(x + max_width - 1, y + row, color);
            }
        }
    }
}

/**
 * @brief   Fill the display buffer with one color.
 * @param   color The color to set.
 */
void eink_fill_color(const eink_color_t color) {
    // set the buffer by memsetting bytes, setting two pixels at a time
    memset(eink_buffer, ((color << 4) | color) & 0xff, EINK_BUFFER_SIZE);
}

/**
 * @brief   Send a full screen region of the eighth "CLEAN" color.
 *
 * This is recommended before changing the displayed image to avoid ghosting artifacts.
 */
void eink_clean(eink_update_cb_t callback) {
    // set callback
    eink_clean_cb = callback;
    // set resolution
    eink_set_tres();
    // start data
    eink_command(EINK_COMMAND_DATA_START);
    // send data
    static const uint32_t clean_eight_pixels = 0x77777777;
    for (uint32_t i = 0; i < EINK_IMAGE_BYTES / sizeof(uint32_t); i++) {
        eink_data((uint8_t*)&clean_eight_pixels, sizeof(uint32_t));
    }
    // power on screen
    eink_command(EINK_COMMAND_POWER_ON);
    eink_wait_busy(true);
    // stop data
    eink_command(EINK_COMMAND_DATA_STOP);
    // eink_wait_busy(true);
    eink_wait_busy_interrupt(true, eink_clean_finish_cb);
    /*
    // power off screen
    eink_command(EINK_COMMAND_POWER_OFF);
    eink_wait_busy(false);

    // wait for screen to update
    //sleep_ms(EINK_SCREEN_UPDATE_DELAY);
    eink_wait_busy(true);
    */
}

/**
 * @brief    Send contents of image buffer to the display.
 */
void eink_update(eink_update_cb_t update_cb, eink_prep_data_cb_t prep_cb) {
    eink_updating = true;
    eink_update_cb = update_cb;
    eink_data_cb = prep_cb;
    // clean the screen, then start the update
    eink_clean(eink_update_start_cb);
}

/**
 * @brief    Put display into deepsleep.
 *
 * Use eink_reset() to wake up.
 */
void eink_sleep() {
    HAL_Delay(EINK_SLEEP_DELAY);

    static const uint8_t deepsleep_data = 0xa5;
    eink_command(EINK_COMMAND_DEEPSLEEP);
    eink_data(&deepsleep_data, 1);

    HAL_Delay(EINK_SLEEP_DELAY);

    // set reset pin low
    HAL_GPIO_WritePin(EINK_RST_GPIO_Port, EINK_RST_Pin, GPIO_PIN_RESET);
}

/**
 * @brief   Trigger the reset pin.
 */
void eink_trigger_reset() {
    // trigger reset pin
    HAL_GPIO_WritePin(EINK_RST_GPIO_Port, EINK_RST_Pin, GPIO_PIN_SET);
    HAL_Delay(EINK_SCREEN_UPDATE_DELAY);
    HAL_GPIO_WritePin(EINK_RST_GPIO_Port, EINK_RST_Pin, GPIO_PIN_RESET);
    HAL_Delay(EINK_RESET_HOLD_TIME);
    HAL_GPIO_WritePin(EINK_RST_GPIO_Port, EINK_RST_Pin, GPIO_PIN_SET);
    HAL_Delay(EINK_SCREEN_UPDATE_DELAY);
}

/**
 * @brief   Get direct pointer to image buffer.
 * @return  Pointer to buffer array.
 */
eink_buffer_t* eink_get_buffer() {
    return &eink_buffer;
}

/**
 * @brief   Task to call repeatedly to handle the busy interrupt.
 */
bool eink_is_updating() {
    return eink_updating;
}

/**
 * @brief   Task to call repeatedly to handle the busy interrupt.
 */
void eink_task() {
    if (eink_busy_flag) {
        if (eink_busy_cb != NULL && eink_busy_state == eink_busy_waiting_state) {
            eink_busy_cb();
        }
        eink_busy_flag = false;
    }
}


/**
 * @brief   Block until busy pin is in a desired state.
 * @param   state State of the BUSY lain to wait for.
 */
static void eink_wait_busy(const bool state) {
    while (HAL_GPIO_ReadPin(EINK_BUSY_GPIO_Port, EINK_BUSY_Pin) != state) {
    };
}

/**
 * @brief   Setup interrupt when busy switches to the desired state.
 * @param   state State of the BUSY line to trigger an interrupt.
 * @param   callback Callback to run when the state change happens.
 */
static void eink_wait_busy_interrupt(const bool state, eink_update_cb_t callback) {
    // set callback
    eink_busy_cb = callback;
    eink_busy_waiting_state = state;
}

/**
 * @brief    Send a command byte.
 * @param    cmd The command.
 */
static void eink_command(const eink_command_t cmd) {
    // set pin to command mode
    HAL_GPIO_WritePin(EINK_DC_GPIO_Port, EINK_DC_Pin, EINK_COMMAND);

    // chip select
    HAL_GPIO_WritePin(EINK_CS_GPIO_Port, EINK_CS_Pin, GPIO_PIN_RESET);

    // write command
    HAL_SPI_Transmit(eink_spi, (uint8_t*)&cmd, 1, 10);

    // chip unselect
    HAL_GPIO_WritePin(EINK_CS_GPIO_Port, EINK_CS_Pin, GPIO_PIN_SET);
}

/**
 * @brief    Send data bytes.
 * @param    data Pointer to the first data byte to be sent.
 * @param    length  Amount of data to send.
 */
static void eink_data(const uint8_t* data, const size_t length) {
    // set DC pin to data mode
    HAL_GPIO_WritePin(EINK_DC_GPIO_Port, EINK_DC_Pin, EINK_DATA);

    // chip select
    HAL_GPIO_WritePin(EINK_CS_GPIO_Port, EINK_CS_Pin, GPIO_PIN_RESET);
    // write data
    // TODO: DMA
    HAL_SPI_Transmit(eink_spi, (uint8_t*)data, length, 500);

    // chip unselect
    HAL_GPIO_WritePin(EINK_CS_GPIO_Port, EINK_CS_Pin, GPIO_PIN_SET);
}

/**
 * @brief   Send TRES command to set resolution.
 *
 * Needs to be set before updating screen every time.
 */
static void eink_set_tres() {
    // set TRES resolution setting
    static const uint8_t tres_setting[4] = {0x02, 0x58, 0x01, 0xc0};
    eink_command(EINK_COMMAND_TRES);
    eink_data(tres_setting, 4);
}

/**
 * @brief   Internal callback called to finish off the clean procedure.
 */
static void eink_clean_finish_cb() {
    // finish off clean procedure
    // power off screen
    eink_command(EINK_COMMAND_POWER_OFF);
    eink_wait_busy(false);

    // wait for screen to update
    eink_wait_busy(true);

    // run callback
    if (eink_clean_cb != NULL) {
        eink_clean_cb();
        eink_clean_cb = NULL;
    }
}

/**
 * @brief   Internal callback called after clean to start transfer of screen contents.
 */
static void eink_update_start_cb() {
    // start the screen update procedure
    // set resolution
    eink_set_tres();
    // start data
    eink_command(EINK_COMMAND_DATA_START);
    // send data
    for (size_t i = 0; i < EINK_BUFFER_PARTS; i++) {
        // prepare new data
        if (eink_data_cb != NULL) {
            eink_data_cb(i, &eink_buffer);
        }

        eink_data(eink_buffer, EINK_BUFFER_SIZE);
    }
    // power on screen
    eink_command(EINK_COMMAND_POWER_ON);
    eink_wait_busy(true);
    // stop data
    eink_command(EINK_COMMAND_DATA_STOP);
    // setup interrupt to finish callback
    eink_wait_busy_interrupt(true, eink_update_finish_cb);
}

/**
 * @brief   Internal callback called to finish off the update procedure.
 */
static void eink_update_finish_cb() {
    // finish update procedure
    // power off screen
    eink_command(EINK_COMMAND_POWER_OFF);
    //eink_wait_busy(false);
    // wait for screen to update
    // sleep_ms(EINK_SCREEN_UPDATE_DELAY);
    eink_updating = false;

    // call callback
    if (eink_update_cb != NULL) {
        eink_update_cb();
        eink_update_cb = NULL;
    }
}


/**
 * @brief   GPIO interrupt handler
 *
 * Getting this IRQ means the display's BUSY line changed state
 */
void EXTI0_1_IRQHandler(void) {
    if (!__HAL_GPIO_EXTI_GET_IT(EINK_BUSY_Pin)) return;

    // clear interrupt
    __HAL_GPIO_EXTI_CLEAR_IT(EINK_BUSY_Pin);
    HAL_NVIC_ClearPendingIRQ(EXTI0_1_IRQn);
    // call callback
    eink_busy_flag = true;
    eink_busy_state = HAL_GPIO_ReadPin(EINK_BUSY_GPIO_Port, EINK_BUSY_Pin);
}