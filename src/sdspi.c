#include "sdspi.h"

#include "main.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stm32l0xx_hal.h>
#include <stm32l0xx_hal_def.h>
#include <stm32l0xx_hal_gpio.h>
#include <stm32l0xx_hal_gpio_ex.h>
#include <stm32l0xx_hal_spi.h>
#include <string.h>

#include "crc16.h"
#include "crc7.h"


// check CD and return SDSPI_ERROR_NO_CARD if not inserted
#define SDSPI_CHECK_INSERTED() \
    if (!sdspi_card_inserted()) return SDSPI_ERROR_NO_CARD;

// undefine card check macro
#ifndef SDSPI_CARD_DETECTION_PIN_ENABLED
#undef SDSPI_CHECK_INSERTED
#define SDSPI_CHECK_INSERTED()
#endif  // SDSPI_CARD_DETECTION_PIN_ENABLED

// check if result is SDSPI_OK, if not return the result
#define SDSPI_CHECK_OK(res) \
    if ((res) != SDSPI_OK) return (res);
#define SDSPI_CHECK_R1(resp, err) \
    if ((resp).r1.byte != 0) return (err);
#define SDSPI_CONVERT_DATA_RESP(resp, err) ((resp) == SDSPI_DATARESPONSE_ACCEPTED ? SDSPI_OK : (err))
#define SDSPI_IS_ACMD(cmd) ((cmd)&0x80)
#define SDSPI_REMOVE_ACMD(acmd) ((acmd)&0x7f)
// expansion macro to create a lookup table cmd -> response length
#define SDSPI_EXPAND_CMD_RESPONSE_LOOKUP(longname, num, response) [SDSPI_CMD(num)] = sizeof(sdspi_r##response##_t),
#define SDSPI_EXPAND_ACMD_RESPONSE_LOOKUP(longname, num, response) \
    [0x80 | SDSPI_CMD(num)] = sizeof(sdspi_r##response##_t),


const static uint8_t DUMMY_BYTE = SDSPI_TX_DATA;
// lookup table mapping command sequence -> response length
const static sdspi_responses_t SDSPI_CMD_RESPONSE_TYPE[] = {SDSPI_CMD_LIST(SDSPI_EXPAND_CMD_RESPONSE_LOOKUP)
                                                                SDSPI_ACMD_LIST(SDSPI_EXPAND_ACMD_RESPONSE_LOOKUP)};

static SPI_HandleTypeDef* sd_spi = NULL;
static sdspi_card_t sdspi_card = SDSPI_CARD_INVALID;
static sdspi_csd_t sdspi_csd = {0};
static uint32_t sdspi_max_block = 0;


static void sdspi_read_data_frame(sdspi_data_frame_t* dest);
static sdspi_dataresponse_t sdspi_write_data_frame(const sdspi_data_frame_t* frame);
static void sdspi_read_data_frame_split(sdspi_datatoken_t* token, uint8_t* data, uint16_t* crc16);
static sdspi_dataresponse_t sdspi_write_data_frame_split(const sdspi_datatoken_t* token, const uint8_t* data);
static sdspi_result_t sdspi_read_block_single(const uint32_t block_addr, uint8_t* dest);
static sdspi_result_t sdspi_read_block_multi(const uint32_t block_addr, uint8_t* dest, const size_t blocks);
static sdspi_result_t sdspi_write_block_single(const uint32_t block_addr, const uint8_t* data);
static sdspi_result_t sdspi_write_block_multi(const uint32_t block_addr, const uint8_t* data, const size_t blocks);
static sdspi_result_t sdspi_get_cid_csd(const sdspi_cmd_t cmd, sdspi_csd_t* buf);
static void spi_write_blocking(SPI_HandleTypeDef* hspi, const uint8_t* data, const size_t length);
static void spi_read_blocking(SPI_HandleTypeDef* hspi, uint8_t* buf, const size_t length);
static inline uint32_t sdspi_scale_address(const uint32_t block_addr);
static inline void sdspi_block_busy();
static inline void sdspi_cs_select();
static inline void sdspi_cs_deselect();
static inline void sdspi_wait_tx();
static inline void sdspi_wait_rx();


/**
 * @brief    Try to initialize a TF Card in SPI mode.
 * @param    spi Pointer to spi instance to use for the card.
 * @return   SDSPI_OK: Card init and reset successful.
 *           SDSPI_ERROR_GENERIC: Generic SPI error.
 *           SDSPI_ERROR_RESPONSE_TIMEOUT: Card did not respond to command within 8 bytes.
 *           SDSPI_ERROR_RESET: Unexpected error in reset sequence.
 *           SDSPI_ERROR_UNKNOWN_CARD: Unknown/Unsupported card detected.
 *           SDSPI_ERROR_READ: Error reading CSD.
 *           SDSPI_ERROR_NO_CARD: No card inserted. (if SDSPI_CARD_DETECTION_PIN_ENABLED defined)
 */
sdspi_result_t sdspi_init(SPI_HandleTypeDef* spi) {
    // init spi interface
    spi->Init.BaudRatePrescaler = SDSPI_INIT_BAUDRATE_SCALE;
    HAL_SPI_Init(spi);

    sd_spi = spi;
    // do a reset sequence
    sdspi_result_t res = sdspi_reset();
    if (res != SDSPI_OK) {
        // reset internal data on failure
        sd_spi = NULL;
        sdspi_card = SDSPI_CARD_INVALID;

        return res;
    }

    // set full speed baudrate
    spi->Init.BaudRatePrescaler = SDSPI_FULL_BAUDRATE_SCALE;
    HAL_SPI_Init(spi);

    return SDSPI_OK;
}

/**
 * @brief    Perform a full reset sequence.
 * @return   SDSPI_OK: Reset successful.
 *           SDSPI_ERROR_GENERIC: Generic SPI error.
 *           SDSPI_ERROR_RESPONSE_TIMEOUT: Card did not respond to command within 8 bytes.
 *           SDSPI_ERROR_RESET: Unexpected error in reset sequence.
 *           SDSPI_ERROR_UNKNOWN_CARD: Unknown/Unsupported card detected.
 *           SDSPI_ERROR_READ: Error reading CSD.
 *           SDSPI_ERROR_NO_CARD: No card inserted. (if SDSPI_CARD_DETECTION_PIN_ENABLED defined)
 */
sdspi_result_t sdspi_reset() {
    SDSPI_CHECK_INSERTED();
    // according to sequence in http://elm-chan.org/docs/mmc/i/sdinit.png
    // TODO: add timeouts?
    uint8_t buffer[10];
    memset(buffer, 0xff, sizeof(buffer));

    // send 80 dummy clock cycles
    sdspi_cs_select();
    spi_write_blocking(sd_spi, buffer, sizeof(buffer));
    // spi_read_blocking(sd_spi, SDSPI_TX_DATA, buffer, 10);
    sdspi_cs_deselect();
    // spi_write_read_blocking(sd_spi, buffer, dest, 10);

    sdspi_result_t res = SDSPI_ERROR_GENERIC;
    sdspi_response_t resp = {0};
    // send CMD0
    res = sdspi_send_command(SDSPI_CMD0, 0, &resp, false);
    SDSPI_CHECK_OK(res);

    // check R1 response
    if (!resp.r1.bits.idle) return SDSPI_ERROR_RESET;

    // send CMD8 to detect card type
    res = sdspi_send_command(SDSPI_CMD8, SDSPI_IF_COND, &resp, false);

    // check illegal command to determine SD version
    if (res != SDSPI_OK || resp.r1.bits.illegal_cmd) {
        // card may be SD v1 or MMC v3
        // send ACMD41
        do {
            res = sdspi_send_command(SDSPI_CMD_APP_CMD, 0, &resp, false);
            SDSPI_CHECK_OK(res);
            res = sdspi_send_command(SDSPI_ACMD41, 0, &resp, false);
            if (res != SDSPI_OK || resp.r1.bits.illegal_cmd) {
                // MMC v3 maybe
                do {
                    res = sdspi_send_command(SDSPI_CMD1, 0, &resp, false);
                    // card is unknown
                    if (res != SDSPI_OK || resp.r1.bits.illegal_cmd) {
                        sdspi_card = SDSPI_CARD_INVALID;
                        return SDSPI_ERROR_UNKNOWN_CARD;
                    }
                } while (resp.r1.bits.idle);
                // card is MMC v3
                sdspi_card = SDSPI_CARD_MMC_3;
                break;
            }
        } while (resp.r1.bits.idle);
        // ACMD41 returned 0
        // SD v1
        sdspi_card = SDSPI_CARD_SD_1;
    } else if (sdspi_convert_r3(resp).ocr != SDSPI_IF_COND) {
        // card accepted CMD8, but did not return 0x1aa, card is rejected
        sdspi_card = SDSPI_CARD_INVALID;
        return SDSPI_ERROR_UNKNOWN_CARD;
    } else {
        // card accepted CMD8 and returned 0x1aa
        // card is SD v2
        do {
            // send ACMD41 with HSC bit
            res = sdspi_send_command(SDSPI_CMD_APP_CMD, 0, &resp, false);
            SDSPI_CHECK_OK(res);
            res = sdspi_send_command(SDSPI_ACMD41, SDSPI_HSC_BIT, &resp, false);
            if (res != SDSPI_OK) {
                // error in command
                sdspi_card = SDSPI_CARD_INVALID;
                return SDSPI_ERROR_UNKNOWN_CARD;
            }
        } while (resp.r1.bits.idle);
        // card is SD v2
        sdspi_card = SDSPI_CARD_SD_2;
    }

    // set sector size for cards < SDHC
    switch (sdspi_card) {
        case SDSPI_CARD_SD_2: {
            // determine if SC or HC/XC
            res = sdspi_send_command(SDSPI_CMD58, 0, &resp, false);
            SDSPI_CHECK_OK(res);
            // check ccs bit
            if (sdspi_convert_r3(resp).ocr & SDSPI_CCS_BIT) {
                sdspi_card = SDSPI_CARD_SDHC_2;
                break;
            } else {
                sdspi_card = SDSPI_CARD_SDSC_2;
                // SDSC is byte addressed and also needs to set block size, fall through
            }
        }
        case SDSPI_CARD_SDSC_2:
        case SDSPI_CARD_SD_1:
        case SDSPI_CARD_MMC_3: {
            // set block size to 512 bytes
            res = sdspi_send_command(SDSPI_CMD16, SDSPI_BLOCK_SIZE, &resp, false);
            SDSPI_CHECK_OK(res);
            break;
        }
        case SDSPI_CARD_SDHC_2:
            // SDHC cards are done here
            break;
        case SDSPI_CARD_INVALID:
            return SDSPI_ERROR_UNKNOWN_CARD;
        default:
            return SDSPI_ERROR_GENERIC;
    }

    // read csd
    res = sdspi_get_csd(&sdspi_csd);
    SDSPI_CHECK_OK(res);

    // calculate maximum number of blocks
    sdspi_max_block = sdspi_get_max_capacity_blocks(&sdspi_csd);

    return SDSPI_OK;
}

/**
 * @brief    Send a CMD to the card.
 * @param    cmd Command or App Command to send.
 * @param    arg Command argument.
 * @param    res Pointer to reasponse struct.
 * @param    hold_cs Whether CS pin should stay asserted after exiting the function.
 * @return   SDSPI_OK: Command was sent successfully.
 *           SDSPI_ERROR_GENERIC: Generic SPI error.
 *           SDSPI_ERROR_RESPONSE_TIMEOUT: Card did not respond to command within 8 bytes.
 *           SDSPI_ERROR_NO_CARD: No card inserted. (if SDSPI_CARD_DETECTION_PIN_ENABLED defined)
 */
sdspi_result_t sdspi_send_command(const sdspi_cmd_t cmd, const uint32_t arg, sdspi_response_t* res, bool hold_cs) {
    SDSPI_CHECK_INSERTED();
    sdspi_cmd_frame_t data = {
        .cmd = SDSPI_REMOVE_ACMD(cmd),
        .arg = __builtin_bswap32(arg),  // need to transfer arg in big endian
        .crc7 = 0,
        .end = 1  // end bit always 1
    };

    // calculate crc over command and argument
    data.crc7 = crc7_get((uint8_t*)&data, sizeof(cmd) + sizeof(arg));

    // write command
    sdspi_cs_select();
    /*if (spi_write_blocking(sd_spi, (uint8_t*)&data, sizeof(data)) < sizeof(data)) {
        sdspi_cs_deselect();
        return SDSPI_ERROR_GENERIC;
    }*/
    spi_write_blocking(sd_spi, (uint8_t*)&data, sizeof(data));

    if (cmd == SDSPI_CMD_STOP_TRANSMISSION) {
        // ignore first byte of response
        uint8_t dummy = SDSPI_TX_DATA;
        spi_read_blocking(sd_spi, &dummy, 1);
    }

    // read single bytes until actual response is detected
    // ncr count ranges from 0 to 8 for SD, 1 to 8 for MMC
    uint8_t* ncr_byte = res->raw;
    uint8_t ncr_count = 0;
    do {
        if (ncr_count > 8) {
            sdspi_cs_deselect();
            // check if card is not inserted and return NO_DEVICE error instead
            SDSPI_CHECK_INSERTED();
            return SDSPI_ERROR_RESPONSE_TIMEOUT;
        }
        *ncr_byte = SDSPI_TX_DATA;
        spi_read_blocking(sd_spi, ncr_byte, 1);
        ncr_count++;
    } while (*ncr_byte & 0x80);

    // read remaining response
    spi_read_blocking(sd_spi, res->raw + 1, SDSPI_CMD_RESPONSE_TYPE[cmd] - 1);

    // deselect cs if no more data expected
    if (!hold_cs) sdspi_cs_deselect();

    return SDSPI_OK;
}

/**
 * @brief    Write data to an arbitrary position of the card.
 * @param    block_addr Address of first block.
 * @param    block_offset Byte offset of data relative to first block.
 * @param    data Pointer to data to write.
 * @param    length_bytes Number of data bytes that should be written.
 * @return   SDSPI_OK: Write successful.
 *           SDSPI_ERROR_GENERIC: Generic SPI error.
 *           SDSPI_ERROR_RESPONSE_TIMEOUT: Card did not respond to command within 8 bytes.
 *           SDSPI_ERROR_READ: Error reading block(s).
 *           SDSPI_ERROR_WRITE: Error writing block(s).
 *           SDSPI_ERROR_NO_CARD: No card inserted. (if SDSPI_CARD_DETECTION_PIN_ENABLED defined)
 *           SDSPI_ERROR_INVALID_ARGS: Write address range is outside of card capacity.
 */
sdspi_result_t sdspi_write(const uint32_t block_addr, const uint16_t block_offset, const uint8_t* data,
                           const size_t length_bytes) {
    SDSPI_CHECK_INSERTED();
    const uint16_t offset = block_offset % SDSPI_BLOCK_SIZE;
    sdspi_result_t res = SDSPI_ERROR_GENERIC;

    // handle small writes seperate
    if (length_bytes < SDSPI_BLOCK_SIZE) {
        if (offset + length_bytes > SDSPI_BLOCK_SIZE) {
            // read two blocks
            uint8_t block_buffer[SDSPI_BLOCK_SIZE * 2] = {0};
            res = sdspi_read_block_multi(block_addr, block_buffer, 2);
            SDSPI_CHECK_OK(res);

            // copy data to block buffer
            memcpy(block_buffer + offset, data, length_bytes);

            // write blocks
            res = sdspi_write_block_multi(block_addr, block_buffer, 2);
            SDSPI_CHECK_OK(res);
            return SDSPI_OK;
        } else {
            // only one block required
            uint8_t block_buffer[SDSPI_BLOCK_SIZE] = {0};
            res = sdspi_read_block_single(block_addr, block_buffer);
            SDSPI_CHECK_OK(res);

            // copy data to block buffer
            memcpy(block_buffer + offset, data, length_bytes);

            // write blocks
            res = sdspi_write_block_single(block_addr, block_buffer);
            SDSPI_CHECK_OK(res);
            return SDSPI_OK;
        }
    }

    // partial length of last block, 0 if last block aligns perfectly
    const size_t last_block_length = (length_bytes + offset) % SDSPI_BLOCK_SIZE;
    // number of blocks that need to be written
    size_t blocks_left = (length_bytes + offset) / SDSPI_BLOCK_SIZE + (last_block_length != 0);
    // check if write is possible
    if (block_addr + blocks_left >= sdspi_max_block) return SDSPI_ERROR_INVALID_ARGS;

    // buffer for the first and last blocks
    uint8_t block_buffer[SDSPI_BLOCK_SIZE] = {0};
    size_t blocks_written = 0;


    if (offset != 0) {
        // read current contents of block
        res = sdspi_read_block_single(block_addr, block_buffer);
        SDSPI_CHECK_OK(res);

        // copy data into block buffer
        memcpy(block_buffer + offset, data, SDSPI_BLOCK_SIZE - offset);

        // write block buffer
        res = sdspi_write_block_single(block_addr, block_buffer);
        SDSPI_CHECK_OK(res);

        blocks_left--;
        blocks_written++;
    }

    // may be done after first block with offset
    if (blocks_left == 0) {
        return SDSPI_OK;
    }

    // write all possible blocks in one go
    // starting from data at an offset, if first block was written previously
    // if offset is 0, start at start of data
    res = sdspi_write_block_multi(block_addr + blocks_written, data + ((SDSPI_BLOCK_SIZE - offset) % SDSPI_BLOCK_SIZE),
                                  blocks_left - (last_block_length != 0));
    SDSPI_CHECK_OK(res);
    // track written blocks
    blocks_written += blocks_left - (last_block_length != 0);
    blocks_left = (last_block_length != 0);

    // write last block, if needed
    if (blocks_left != 0) {
        // read contents of last block
        res = sdspi_read_block_single(block_addr + blocks_written, block_buffer);
        SDSPI_CHECK_OK(res);

        // copy leftover data
        memcpy(block_buffer, data + (SDSPI_BLOCK_SIZE * blocks_written) - offset, last_block_length);

        // write block
        res = sdspi_write_block_single(block_addr + blocks_written, block_buffer);
        SDSPI_CHECK_OK(res);
    }

    return SDSPI_OK;
}

/**
 * @brief    Read data from an arbitrary position of the card.
 * @param    block_addr Address of first block.
 * @param    block_offset Byte offset of data relative to first block.
 * @param    data Pointer to destination of read data.
 * @param    length_bytes Number of data bytes that should be read.
 * @return   SDSPI_OK: Write successful.
 *           SDSPI_ERROR_GENERIC: Generic SPI error.
 *           SDSPI_ERROR_RESPONSE_TIMEOUT: Card did not respond to command within 8 bytes.
 *           SDSPI_ERROR_READ: Error reading block(s).
 *           SDSPI_ERROR_NO_CARD: No card inserted. (if SDSPI_CARD_DETECTION_PIN_ENABLED defined)
 *           SDSPI_ERROR_INVALID_ARGS: Read address range is outside of card capacity.
 */
sdspi_result_t sdspi_read(const uint32_t block_addr, const uint16_t block_offset, uint8_t* data,
                          const size_t length_bytes) {
    SDSPI_CHECK_INSERTED();
    const uint16_t offset = block_offset % SDSPI_BLOCK_SIZE;
    sdspi_result_t res = SDSPI_ERROR_GENERIC;

    // handle small reads seperate
    if (length_bytes < SDSPI_BLOCK_SIZE) {
        if (offset + length_bytes > SDSPI_BLOCK_SIZE) {
            // read two blocks
            uint8_t block_buffer[SDSPI_BLOCK_SIZE * 2] = {0};
            res = sdspi_read_block_multi(block_addr, block_buffer, 2);
            SDSPI_CHECK_OK(res);

            // copy data from block buffer
            memcpy(data, block_buffer + offset, length_bytes);
            return SDSPI_OK;
        } else {
            // only one block required
            uint8_t block_buffer[SDSPI_BLOCK_SIZE] = {0};
            res = sdspi_read_block_single(block_addr, block_buffer);
            SDSPI_CHECK_OK(res);

            // copy data
            memcpy(data, block_buffer + offset, length_bytes);
            return SDSPI_OK;
        }
    }

    // partial length of last block, 0 if last block aligns perfectly
    const size_t last_block_length = (length_bytes + offset) % SDSPI_BLOCK_SIZE;
    // number of blocks that need to be read
    size_t blocks_left = (length_bytes + offset) / SDSPI_BLOCK_SIZE + (last_block_length != 0);
    // check if read is possible
    if (block_addr + blocks_left >= sdspi_max_block) return SDSPI_ERROR_INVALID_ARGS;
    // buffer for the first and last blocks
    uint8_t block_buffer[SDSPI_BLOCK_SIZE] = {0};
    size_t blocks_read = 0;

    if (offset != 0) {
        // get first block with offset
        res = sdspi_read_block_single(block_addr, block_buffer);
        SDSPI_CHECK_OK(res);

        // copy into actual data array
        memcpy(data, block_buffer + offset, SDSPI_BLOCK_SIZE - offset);
        blocks_left--;
        blocks_read++;
    }

    // may be done after first block with offset
    if (blocks_left == 0) {
        return SDSPI_OK;
    }

    // read all possible blocks in one go
    res = sdspi_read_block_multi(block_addr + blocks_read, data + ((SDSPI_BLOCK_SIZE - offset) % SDSPI_BLOCK_SIZE), blocks_left - (last_block_length != 0));
    SDSPI_CHECK_OK(res);
    // track read blocks
    blocks_read += blocks_left - (last_block_length != 0);
    blocks_left = (last_block_length != 0);

    // read last block, if needed
    if (blocks_left != 0) {
        res = sdspi_read_block_single(block_addr + blocks_read, block_buffer);
        SDSPI_CHECK_OK(res);

        // copy leftover data
        memcpy(data + (SDSPI_BLOCK_SIZE * blocks_read) - offset, block_buffer, last_block_length);
    }

    return SDSPI_OK;
}

/**
 * @brief    Get current card type.
 * @return   The card type of the currently inited card.
 */
sdspi_card_t sdspi_get_card() {
    return sdspi_card;
}

/**
 * @brief    Whether a card is currently inserted.
 * @return   True: A card is inserted.
 *           False: No card detected.
 *
 * Requires CD pin to be connected.
 */
bool sdspi_card_inserted() {
    //return sdspi_pins.cd != -1 && !gpio_get(sdspi_pins.cd);
    return !HAL_GPIO_ReadPin(CARD_CD_GPIO_Port, CARD_CD_Pin);
}

/**
 * @brief    Read CID of card.
 * @param    cid Pointer to 16-byte buffer to write the cid to.
 * @return   SDSPI_OK: Read CID successfully.
 *           SDSPI_ERROR_GENERIC: Generic SPI error.
 *           SDSPI_ERROR_RESPONSE_TIMEOUT: Card did not respond to command within 8 bytes.
 *           SDSPI_ERROR_READ: Error reading cid.
 *           SDSPI_ERROR_NO_CARD: No card inserted. (if SDSPI_CARD_DETECTION_PIN_ENABLED defined)
 */
sdspi_result_t sdspi_get_cid(sdspi_cid_t* cid) {
    return sdspi_get_cid_csd(SDSPI_CMD_SEND_CID, cid);
}

/**
 * @brief    Read CSD of card.
 * @param    csd Pointer to 16-byte buffer to write the csd to.
 * @return   SDSPI_OK: Read CSD successfully.
 *           SDSPI_ERROR_GENERIC: Generic SPI error.
 *           SDSPI_ERROR_RESPONSE_TIMEOUT: Card did not respond to command within 8 bytes.
 *           SDSPI_ERROR_READ: Error reading csd.
 *           SDSPI_ERROR_NO_CARD: No card inserted. (if SDSPI_CARD_DETECTION_PIN_ENABLED defined)
 */
sdspi_result_t sdspi_get_csd(sdspi_csd_t* csd) {
    return sdspi_get_cid_csd(SDSPI_CMD_SEND_CSD, csd);
}

/**
 * @brief    Calculate maximum capacity in blocks from a csd.
 * @param    csd Pointer to 16-byte buffer containing the CSD.
 * @return   Number of blocks.
 */
uint32_t sdspi_get_max_capacity_blocks(sdspi_csd_t* csd) {
    // get csd version
    sdspi_csd_version_t ver =
        ((*csd)[SDSPI_CID_CSD_SIZE - (SDSPI_CSD1_CSD_VERSION_POS / 8) - 1] >> (SDSPI_CSD1_CSD_VERSION_POS % 8)) &
        ((1 << SDSPI_CSD1_CSD_VERSION_LENGTH) - 1);
    if (ver == SDSPI_CSD_VERSION_1) {
        // calculate from C_SIZE, C_SIZE_MULT and BLOCK_LEN
        const uint8_t c_size_mult =
            ((*csd)[SDSPI_CID_CSD_SIZE - (SDSPI_CSD1_C_SIZE_MULT_POS / 8) - 1] >> (SDSPI_CSD1_C_SIZE_MULT_POS % 8)) &
            ((1 << SDSPI_CSD1_C_SIZE_MULT_LENGTH) - 1);
        const uint8_t read_block_len =
            ((*csd)[SDSPI_CID_CSD_SIZE - (SDSPI_CSD1_READ_BL_LEN_POS / 8) - 1] >> (SDSPI_CSD1_READ_BL_LEN_POS % 8)) &
            ((1 << SDSPI_CSD1_READ_BL_LEN_LENGTH) - 1);

        const uint8_t c_size_start_byte = SDSPI_CID_CSD_SIZE - (SDSPI_CSD1_C_SIZE_POS / 8) - 1;
        const uint8_t c_size_shift = 8 - (SDSPI_CSD1_C_SIZE_POS % 8);
        // get c_size from the 3 bytes it occupies
        const uint16_t c_size =
            (((*csd)[c_size_start_byte] >> c_size_shift) | ((*csd)[c_size_start_byte - 1] << (8 - c_size_shift)) |
             ((*csd)[c_size_start_byte - 2] << (16 - c_size_shift))) &
            ((1 << SDSPI_CSD1_C_SIZE_LENGTH) - 1);

        const uint16_t mult = 1 << (c_size_mult + 2);
        const uint16_t block_len = 1 << read_block_len;
        // calculate total number of bytes
        const uint32_t bytes = (c_size + 1) * mult * block_len;

        // return number of 512 byte blocks
        return bytes / SDSPI_BLOCK_SIZE;
    } else if (ver == SDSPI_CSD_VERSION_2) {
        // calculate from C_SIZE
        // C_SIZE is byte aligned in CSD2
        const uint8_t c_size_start_byte = SDSPI_CID_CSD_SIZE - (SDSPI_CSD2_C_SIZE_POS / 8) - 1;
        const uint32_t c_size =
            ((*csd)[c_size_start_byte] | ((*csd)[c_size_start_byte - 1] << 8) | ((*csd)[c_size_start_byte - 2] << 16)) &
            ((1 << SDSPI_CSD2_C_SIZE_LENGTH) - 1);

        // calculate number of 512 byte blocks
        return (c_size + 1) * SDSPI_CSD2_C_SIZE_MULT;
    } else {
        // unsupported version, report capacity 0
        return 0;
    }
}

/**
 * @brief    Get capacity of current card (calculated in reset sequence).
 * @return   Number of blocks.
 */
uint32_t sdspi_capacity_blocks() {
    if (!sdspi_card_inserted())
        return 0;
    else
        return sdspi_max_block;
}


/**
 * @brief    Read one data frame.
 * @param    dest Pointer to data frame struct.
 */
static void sdspi_read_data_frame(sdspi_data_frame_t* dest) {
    uint16_t tmp_crc16 = 0;
    sdspi_read_data_frame_split(&dest->frame.token, dest->frame.data, &tmp_crc16);
    dest->frame.crc16 = tmp_crc16;
}

/**
 * @brief    Write one data frame.
 * @param    frame Pointer to data frame struct.
 * @return   Data Response Token
 */
static sdspi_dataresponse_t sdspi_write_data_frame(const sdspi_data_frame_t* frame) {
    return sdspi_write_data_frame_split(&frame->frame.token, frame->frame.data);
}

/**
 * @brief    Read one data frame.
 * @param    token Pointer to data token.
 * @param    data Pointer to data of data frame.
 * @param    crc16 Pointer to crc16 of data frame.
 */
static void sdspi_read_data_frame_split(sdspi_datatoken_t* token, uint8_t* data, uint16_t* crc16) {
    // read bytes until a data token is found
    uint8_t* data_token = (uint8_t*)token;
    do {
        *data_token = SDSPI_TX_DATA;
        spi_read_blocking(sd_spi, data_token, 1);
    } while (*data_token != SDSPI_DATATOKEN_REGULAR);

    // data token received, read data bytes
    spi_read_blocking(sd_spi, data, sizeof(sdspi_data_frame_t) - 3);
    // read crc16
    spi_read_blocking(sd_spi, (uint8_t*)crc16, sizeof(uint16_t));

    // swap crc to little endian
    *crc16 = __builtin_bswap16(*crc16);
}

/**
 * @brief    Write one data frame.
 * @param    token Pointer to data token.
 * @param    data Pointer to data of data frame.
 * @return   Data Reponse Token
 */
static sdspi_dataresponse_t sdspi_write_data_frame_split(const sdspi_datatoken_t* token, const uint8_t* data) {
    // calculate crc16 (in big endian)
    uint16_t crc16 = __builtin_bswap16(crc16_get(data, SDSPI_BLOCK_SIZE));

    // write data token
    spi_write_blocking(sd_spi, token, 1);
    // write the data frame without crc
    spi_write_blocking(sd_spi, data, SDSPI_BLOCK_SIZE);
    // write swapped crc
    spi_write_blocking(sd_spi, (uint8_t*)&crc16, sizeof(crc16));

    // get data response byte
    sdspi_dataresponse_t resp;
    resp = SDSPI_TX_DATA;
    spi_read_blocking(sd_spi, (uint8_t*)&resp, 1);

    // wait until card is done being busy
    sdspi_block_busy();

    return resp & SDSPI_DATA_RESPONSE_BITS_MASK;
}

/**
 * @brief    Read a single block. (CMD17)
 * @param    block_addr Block to read.
 * @param    dest Pointer to write the read data to. Must be at least SDSPI_BLOCK_SIZE long.
 * @return   SDSPI_OK: Read successful.
 *           SDSPI_ERROR_GENERIC: Generic SPI error.
 *           SDSPI_ERROR_RESPONSE_TIMEOUT: Card did not respond to command within 8 bytes.
 *           SDSPI_ERROR_READ: Block read failed.
 *           SDSPI_ERROR_NO_CARD: No card inserted. (if SDSPI_CARD_DETECTION_PIN_ENABLED defined)
 */
static sdspi_result_t sdspi_read_block_single(const uint32_t block_addr, uint8_t* dest) {
    SDSPI_CHECK_INSERTED();
    // TODO: check if address inside capacity range
    const uint32_t addr = sdspi_scale_address(block_addr);
    sdspi_response_t resp = {0};
    sdspi_result_t res = SDSPI_ERROR_GENERIC;
    // send read single block command and hold cs for data frames
    res = sdspi_send_command(SDSPI_CMD_READ_SINGLE_BLOCK, addr, &resp, true);
    SDSPI_CHECK_OK(res);

    // read command failed?
    SDSPI_CHECK_R1(resp, SDSPI_ERROR_READ);

    sdspi_datatoken_t token = 0;
    uint16_t crc16 = 0;

    // read incoming data packet
    // TODO: handle error tokens?
    sdspi_read_data_frame_split(&token, dest, &crc16);

    // deselect cs
    sdspi_cs_deselect();

    return SDSPI_OK;
}

/**
 * @brief    Read a multiple blocks. (CMD18)
 * @param    block_addr Block to read.
 * @param    dest Pointer to write the read data to. Must be at least SDSPI_BLOCK_SIZE * blocks long.
 * @param    blocks Number of blocks to read.
 * @return   SDSPI_OK: Read successful.
 *           SDSPI_ERROR_GENERIC: Generic SPI error.
 *           SDSPI_ERROR_RESPONSE_TIMEOUT: Card did not respond to command within 8 bytes.
 *           SDSPI_ERROR_READ: Block read failed.
 *           SDSPI_ERROR_NO_CARD: No card inserted. (if SDSPI_CARD_DETECTION_PIN_ENABLED defined)
 */
static sdspi_result_t sdspi_read_block_multi(const uint32_t block_addr, uint8_t* dest, const size_t blocks) {
    SDSPI_CHECK_INSERTED();
    // don't do anything if nothing needs to be read
    if (blocks == 0) return SDSPI_OK;
    if (blocks == 1) return sdspi_read_block_single(block_addr, dest);

    // TODO: check if address inside capacity range
    const uint32_t addr = sdspi_scale_address(block_addr);
    sdspi_response_t resp = {0};
    sdspi_result_t res = SDSPI_ERROR_GENERIC;

    // MMC needs a CMD23 to specify amount of read blocks
    if (sdspi_card == SDSPI_CARD_MMC_3) {
        res = sdspi_send_command(SDSPI_CMD_SET_BLOCK_COUNT, blocks, &resp, false);
        SDSPI_CHECK_OK(res);

        SDSPI_CHECK_R1(resp, SDSPI_ERROR_READ);
    }

    // send read multi block command and hold cs for data frames
    res = sdspi_send_command(SDSPI_CMD_READ_MULTIPLE_BLOCK, addr, &resp, true);
    SDSPI_CHECK_OK(res);

    // read command failed?
    SDSPI_CHECK_R1(resp, SDSPI_ERROR_READ);

    sdspi_datatoken_t token = 0;
    uint16_t crc16 = 0;

    // read incoming data packets
    for (size_t i = 0; i < blocks; i++) {
        // TODO: handle error tokens?
        sdspi_read_data_frame_split(&token, dest + SDSPI_BLOCK_SIZE * i, &crc16);
    }

    // send stop command
    res = sdspi_send_command(SDSPI_CMD_STOP_TRANSMISSION, 0, &resp, true);
    SDSPI_CHECK_OK(res);
    SDSPI_CHECK_R1(resp, SDSPI_ERROR_READ);

    // block until card is not busy
    sdspi_block_busy();
    // deselect cs
    sdspi_cs_deselect();

    return SDSPI_OK;
}

/**
 * @brief    Write a single block. (CMD24)
 * @param    block_addr Block to write.
 * @param    dest Pointer to data to write. Must be at least SDSPI_BLOCK_SIZE long.
 * @return   SDSPI_OK: Write successful.
 *           SDSPI_ERROR_GENERIC: Generic SPI error.
 *           SDSPI_ERROR_RESPONSE_TIMEOUT: Card did not respond to command within 8 bytes.
 *           SDSPI_ERROR_WRITE: Block write failed.
 *           SDSPI_ERROR_NO_CARD: No card inserted. (if SDSPI_CARD_DETECTION_PIN_ENABLED defined)
 */
static sdspi_result_t sdspi_write_block_single(const uint32_t block_addr, const uint8_t* data) {
    SDSPI_CHECK_INSERTED();
    // TODO: check capacity boundary
    const uint32_t addr = sdspi_scale_address(block_addr);
    sdspi_response_t resp = {0};
    sdspi_result_t res = SDSPI_ERROR_GENERIC;

    // send single block write command
    res = sdspi_send_command(SDSPI_CMD_WRITE_BLOCK, addr, &resp, true);
    SDSPI_CHECK_OK(res);
    // write fail?
    SDSPI_CHECK_R1(resp, SDSPI_ERROR_WRITE);

    // need to write some 0xff bytes before writing data
    uint8_t stuff_buffer[1] = {0xff};
    spi_write_blocking(sd_spi, stuff_buffer, sizeof(stuff_buffer));

    // write data packet
    static const sdspi_datatoken_t token = SDSPI_DATATOKEN_REGULAR;
    sdspi_dataresponse_t data_resp = sdspi_write_data_frame_split(&token, data);

    // wait busy
    sdspi_block_busy();

    // deselect cs
    sdspi_cs_deselect();

    return SDSPI_CONVERT_DATA_RESP(data_resp, SDSPI_ERROR_WRITE);
}

/**
 * @brief    Write a multiple blocks. (CMD24)
 * @param    block_addr Block to write.
 * @param    dest Pointer to data to write. Must be at least SDSPI_BLOCK_SIZE * blocks long.
 * @param    blocks Number of blocks to write.
 * @return   SDSPI_OK: Write successful.
 *           SDSPI_ERROR_GENERIC: Generic SPI error.
 *           SDSPI_ERROR_RESPONSE_TIMEOUT: Card did not respond to command within 8 bytes.
 *           SDSPI_ERROR_WRITE: Block write failed.
 *           SDSPI_ERROR_NO_CARD: No card inserted. (if SDSPI_CARD_DETECTION_PIN_ENABLED defined)
 *
 * Blocks will be pre-erased using ACMD23.
 */
static sdspi_result_t sdspi_write_block_multi(const uint32_t block_addr, const uint8_t* data, const size_t blocks) {
    SDSPI_CHECK_INSERTED();
    // don't do anything if no blocks need to be written
    if (blocks == 0) return SDSPI_OK;
    if (blocks == 1) return sdspi_write_block_single(block_addr, data);

    // TODO: check capacity boundary
    //const uint32_t addr = sdspi_scale_address(block_addr);
    sdspi_response_t resp = {0};
    sdspi_result_t res = SDSPI_ERROR_GENERIC;

    // prepare multiple write command by specifying amount of blocks
    if (sdspi_card == SDSPI_CARD_MMC_3) {
        // MMCv3 uses CMD23 for this
        res = sdspi_send_command(SDSPI_CMD_SET_BLOCK_COUNT, blocks, &resp, true);
        SDSPI_CHECK_OK(res);
        SDSPI_CHECK_R1(resp, SDSPI_ERROR_WRITE);
    } else {
        // SD has ACMD23 to pre-erase blocks for faster writes
        res = sdspi_send_command(SDSPI_CMD_APP_CMD, 0, &resp, true);
        SDSPI_CHECK_OK(res);
        SDSPI_CHECK_R1(resp, SDSPI_ERROR_WRITE);
        res = sdspi_send_command(SDSPI_ACMD_SET_WR_BLK_ERASE_COUNT, blocks, &resp, true);
        SDSPI_CHECK_OK(res);
        SDSPI_CHECK_R1(resp, SDSPI_ERROR_WRITE);
    }

    // send multi block write command
    res = sdspi_send_command(SDSPI_CMD_WRITE_MULTIPLE_BLOCK, block_addr, &resp, true);
    SDSPI_CHECK_OK(res);
    SDSPI_CHECK_R1(resp, SDSPI_ERROR_WRITE);

    // need to write some 0xff bytes before writing data
    uint8_t stuff_buffer[1] = {0xff};
    spi_write_blocking(sd_spi, stuff_buffer, sizeof(stuff_buffer));

    static const sdspi_datatoken_t token = SDSPI_DATATOKEN_MULTI_WRITE;
    sdspi_dataresponse_t data_resp = {0};
    uint8_t num_tries = 0;

    // write data packets
    for (size_t i = 0; i < blocks; i++) {
        data_resp = sdspi_write_data_frame_split(&token, data + i * SDSPI_BLOCK_SIZE);

        if (data_resp != SDSPI_DATARESPONSE_ACCEPTED) {
            // return bad data response
            if (num_tries >= SDSPI_MAX_RETRIES) {
                return SDSPI_CONVERT_DATA_RESP(data_resp, SDSPI_ERROR_WRITE);
            }

            // retry this packet
            num_tries++;
            i--;
        }


        // wait busy before sending next packet
        sdspi_block_busy();
    }

    // send stop transaction token
    static const sdspi_datatoken_t stop_token = SDSPI_DATATOKEN_STOP_MULTI_WRITE;
    spi_write_blocking(sd_spi, (uint8_t*)&stop_token, sizeof(sdspi_datatoken_t));

    // ignore one byte
    spi_write_blocking(sd_spi, stuff_buffer, 1);

    // wait busy
    sdspi_block_busy();

    // deselect cs
    sdspi_cs_deselect();

    return SDSPI_CONVERT_DATA_RESP(data_resp, SDSPI_ERROR_WRITE);
}

/**
 * @brief    Read cid or csd.
 * @param    cmd Command to use. Only SDSPI_CMD_SEND_CID and SDSPI_CMD_SEND_CSD are valid.
 * @param    buf Pointer to 16 byte buffer to write the cid/csd to.
 * @return   SDSPI_OK: Read successful.
 *           SDSPI_ERROR_GENERIC: Generic SPI error.
 *           SDSPI_ERROR_RESPONSE_TIMEOUT: Card did not respond to command within 8 bytes.
 *           SDSPI_ERROR_READ: Reading failed.
 *           SDSPI_ERROR_NO_CARD: No card inserted. (if SDSPI_CARD_DETECTION_PIN_ENABLED defined)
 *           SDSPI_ERROR_INVALID_ARGS: Invalid cmd specifed.
 */
static sdspi_result_t sdspi_get_cid_csd(const sdspi_cmd_t cmd, sdspi_csd_t* buf) {
    if (cmd != SDSPI_CMD_SEND_CSD && cmd != SDSPI_CMD_SEND_CID) return SDSPI_ERROR_INVALID_ARGS;
    SDSPI_CHECK_INSERTED();
    sdspi_response_t resp = {0};
    sdspi_result_t res = SDSPI_ERROR_GENERIC;
    // send read single block command and hold cs for data frames
    res = sdspi_send_command(cmd, 0, &resp, true);
    SDSPI_CHECK_OK(res);

    // read command failed?
    SDSPI_CHECK_R1(resp, SDSPI_ERROR_READ);

    // read incoming data packet
    // read bytes until a data token is found
    uint8_t data_token;
    do {
        data_token = SDSPI_TX_DATA;
        spi_read_blocking(sd_spi, &data_token, 1);
    } while (data_token != SDSPI_DATATOKEN_REGULAR);

    // data token received, read data bytes
    spi_read_blocking(sd_spi, *buf, sizeof(sdspi_csd_t));

    // read crc16
    uint16_t crc16 = SDSPI_TX_DATA | SDSPI_TX_DATA << 8;
    spi_read_blocking(sd_spi, (uint8_t*)&crc16, sizeof(uint16_t));

    // deselect cs
    sdspi_cs_deselect();

    return SDSPI_OK;
}

/**
 * @brief    Start an spi write and block until it finishes.
 * @param    hspi HAL spi handle
 * @param    data Buffer of data to transmit
 * @param    length Amount of bytes to transmit
 *
 * Will use DMA unless only one byte needs to be transmitted.
 */
static void spi_write_blocking(SPI_HandleTypeDef* hspi, const uint8_t* data, const size_t length) {
    if (length > 1) {
        HAL_SPI_Transmit_DMA(hspi, (uint8_t*)data, length);
        sdspi_wait_tx();
    } else if (length == 1) {
        HAL_SPI_Transmit(hspi, (uint8_t*)data, length, UINT32_MAX);
    }
}

/**
 * @brief    Start an spi read and block until it finishes.
 * @param    hspi HAL spi handle
 * @param    buf Buffer for data to receife
 * @param    length Amount of bytes to receive
 *
 * Will use DMA unless only one byte needs to be received.
 */
static void spi_read_blocking(SPI_HandleTypeDef* hspi, uint8_t* buf, const size_t length) {
    static const GPIO_InitTypeDef GPIO_OUT = {
        .Pin = CARD_MOSI_Pin,
        .Mode = GPIO_MODE_OUTPUT_PP,
        .Pull = GPIO_PULLUP,
        .Speed = GPIO_SPEED_FREQ_VERY_HIGH,
        .Alternate = 0
    };

    static const GPIO_InitTypeDef GPIO_SPI = {
        .Pin = CARD_MOSI_Pin,
        .Mode = GPIO_MODE_AF_PP,
        .Pull = GPIO_PULLUP,
        .Speed = GPIO_SPEED_FREQ_VERY_HIGH,
        .Alternate = GPIO_AF0_SPI1
    };

    // set to gpio out and set pin HIGH
    HAL_GPIO_Init(CARD_MOSI_GPIO_Port, (GPIO_InitTypeDef*)&GPIO_OUT);
    HAL_GPIO_WritePin(CARD_MOSI_GPIO_Port, CARD_MOSI_Pin, GPIO_PIN_SET);

    // receive data
    if (length > 1) {
        HAL_SPI_Receive_DMA(hspi, buf, length);
        sdspi_wait_rx();
    } else if (length == 1) {
        HAL_SPI_Receive(hspi, buf, length, UINT32_MAX);
    }

    // reset MOSI to SPI function
    HAL_GPIO_WritePin(CARD_MOSI_GPIO_Port, CARD_MOSI_Pin, GPIO_PIN_RESET);
    HAL_GPIO_Init(CARD_MOSI_GPIO_Port, (GPIO_InitTypeDef*)&GPIO_SPI);
}


/**
 * @brief    Scale a block address to a byte address if card type is byte addressed.
 * @param    block_addr Block address.
 * @return   Scaled byte address, if card < SDHC, same block address otherwise.
 */
static inline uint32_t sdspi_scale_address(const uint32_t block_addr) {
    // scale address for byte-addressed cards
    if (sdspi_card < SDSPI_CARD_SDHC_2) {
        return block_addr * SDSPI_BLOCK_SIZE;
    } else {
        return block_addr;
    }
}

/**
 * @brief    Block while card is busy.
 */
static inline void sdspi_block_busy() {
    // read bytes until something non-zero was read
    uint8_t byte;
    do {
        byte = SDSPI_TX_DATA;
        spi_read_blocking(sd_spi, &byte, 1);
    } while (byte == 0);
}

/**
 * @brief    Assert CS pin. (Set CS low.)
 * @param    cs CS pin.
 */
static inline void sdspi_cs_select() {
    HAL_SPI_Transmit(sd_spi, (uint8_t*)&DUMMY_BYTE, 1, HAL_MAX_DELAY);
    HAL_GPIO_WritePin(CARD_CS_GPIO_Port, CARD_CS_Pin, GPIO_PIN_RESET);
    HAL_SPI_Transmit(sd_spi, (uint8_t*)&DUMMY_BYTE, 1, HAL_MAX_DELAY);
}

/**
 * @brief    Deassert CS pin. (Set CS high.)
 * @param    cs CS pin.
 */
static inline void sdspi_cs_deselect() {
    HAL_SPI_Transmit(sd_spi, (uint8_t*)&DUMMY_BYTE, 1, HAL_MAX_DELAY);
    HAL_GPIO_WritePin(CARD_CS_GPIO_Port, CARD_CS_Pin, GPIO_PIN_SET);
    HAL_SPI_Transmit(sd_spi, (uint8_t*)&DUMMY_BYTE, 1, HAL_MAX_DELAY);
}

/**
 * @brief    Block while spi in transmit state.
 */
static inline void sdspi_wait_tx() {
    while (sd_spi->State == HAL_SPI_STATE_BUSY_TX) {};
}

/**
 * @brief    Block while spi in receive state.
 */
static inline void sdspi_wait_rx() {
    while (sd_spi->State == HAL_SPI_STATE_BUSY_RX) {};
}