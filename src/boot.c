#include <stm32l0xx_hal_def.h>
#include <cmsis_gcc.h>
#include <stdint.h>
#include <stm32l073xx.h>
#include <stm32l0xx.h>
#include <stm32l0xx_hal_rcc.h>

#include "eeprom.h"

#define BOOTLOADER_SIZE 1024  // [Byte] maximum size of this bootloader
#define BANK_SIZE (64 * 1024)  // [Byte] size of one flash bank
#define CHECK_SP_RANGE(val) ((val) > 0x20000000 && (val) <= 0x20005004)


int main(void) {
    __ASM ("CPSIE i");  // see erratum 2.1.9 in es0292

    // jump point into system memory to do the bank switching
    void (*switch_banks)(void);
    switch_banks = (void(*)(void))*(unsigned int*)0x1FF00004;

    // magic value in eeprom to denote which bank to use
    volatile uint32_t* bank_magic = (void*)EEPROM_USE_BANK2_MAGIC_ADDRESS;
    const uint32_t* bank2_start = (void*)(FLASH_BASE + BANK_SIZE + BOOTLOADER_SIZE);
    const uint32_t* bank1_start = (void*)(FLASH_BASE + BOOTLOADER_SIZE);
    const uint32_t* bank2_bootloader = (void*)(FLASH_BASE + BANK_SIZE);

    const bool bank2 = READ_BIT(SYSCFG->CFGR1, SYSCFG_CFGR1_UFB);

    SET_BIT(RCC->APB2ENR, (RCC_APB2ENR_SYSCFGEN));
    // if in bank1, then check bank magic and swap banks
    if (!bank2) {
        // swap bank if magic value set and stack pointer is valid
        if (*bank_magic == EEPROM_USE_BANK2_MAGIC && CHECK_SP_RANGE(*bank2_start) && CHECK_SP_RANGE(*bank2_bootloader)) {
            switch_banks();
        }

        // also swap banks if bank1 is not valid
        if (!CHECK_SP_RANGE(*bank1_start)) {
            if (CHECK_SP_RANGE(*bank2_start) && CHECK_SP_RANGE(*bank2_bootloader)) {
                switch_banks();
            } else {
                // bank1 (current) and bank2 not valid, will die here
                __HAL_RCC_GPIOB_CLK_ENABLE();
                GPIOB->MODER &= ~GPIO_MODER_MODE5;
                GPIOB->MODER |= GPIO_MODER_MODE5_0;
                while (1) {
                    // blink LED2 forever
                    SET_BIT(GPIOB->ODR, 1 << 5);
                    for (uint32_t i = 0; i < 200000; i++) {
                        __NOP();
                    }
                    CLEAR_BIT(GPIOB->ODR, 1 << 5);
                    for (uint32_t i = 0; i < 200000; i++) {
                        __NOP();
                    }
                }
            }
        }
    }

    // setup stackpointer and vector table
    __set_MSP(*bank1_start);
    SCB->VTOR = (uint32_t)bank1_start;
    // jump into firmware on bank1
    void (*main_code)(void);
    main_code = (void(*)(void))(*(bank1_start + 1));

    main_code();
}