#include "eeprom.h"

#include <stdint.h>
#include <stm32l0xx.h>
#include <stm32l0xx_hal_flash_ex.h>
#include <string.h>


static volatile uint32_t *const eeprom_magic = (uint32_t*)EEPROM_MAGIC_ADDRESS;
static volatile uint32_t *const bank2_magic = (uint32_t*)EEPROM_USE_BANK2_MAGIC_ADDRESS;


/**
 * @brief   Unlock and initialize eeprom with data from flash.
 * @param   force Overwrite data regardless of magic value.
 */
void eeprom_init(const bool force) {
    // ensure EEPROM is writable
    HAL_FLASHEx_DATAEEPROM_Unlock();

    // copy initial values if magic is not set
    if (*eeprom_magic != EEPROM_MAGIC || force) {
        // linker symbols
        extern uint32_t _init_eeprom;  // first word of eeprom init data in flash
        extern uint32_t _start_eeprom;  // first word in eeprom region
        extern uint32_t _end_eeprom;  // last word in eeprom region

        // number of bytes to be copied
        const size_t length = ((uint32_t)&_end_eeprom) - ((uint32_t)&_start_eeprom);
        memcpy(&_start_eeprom, &_init_eeprom, length);

        // set magic
        *eeprom_magic = EEPROM_MAGIC;
    }
}

/**
 * @brief   Copy main eeprom contents to second bank.
 */
__RAM_FUNC void eeprom_copy_bank() {
    //memcpy((uint8_t*)(EEPROM_BASE + EEPROM_BANK_SIZE), (uint8_t*)EEPROM_BASE, EEPROM_BANK_SIZE);
    uint32_t* src = (uint32_t*)EEPROM_BASE;
    uint32_t* end = (uint32_t*)(EEPROM_BASE + EEPROM_BANK_SIZE);
    while (src != end) {
        volatile uint32_t* dest = EEPROM_BANK_B_PTR(src);
        *dest = *src;

        // sanity
        while (*dest != *src) {
            HAL_FLASHEx_DATAEEPROM_Unlock();
            *dest = *src;
        }

        src++;
    }
}

/**
 * @brief   Set option byte to change memory map.
 *
 * This will switch EEPROM and FLASH banks, effective next reset.
 */
__RAM_FUNC void eeprom_switch_banks() {
    HAL_FLASHEx_DATAEEPROM_Unlock();

    // set eeprom bank magic
    if (READ_BIT(SYSCFG->CFGR1, SYSCFG_CFGR1_UFB)) {
        // switch to bank1
        *bank2_magic = 0;
    } else {
        *bank2_magic = EEPROM_USE_BANK2_MAGIC;
    }

    // copy eeprom bank beforehand
    eeprom_copy_bank();
}