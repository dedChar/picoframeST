/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2019 Ha Thach (tinyusb.org)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

/* This example demonstrates WebUSB as web serial with browser with WebUSB support (e.g Chrome).
 * After enumerated successfully, browser will pop-up notification
 * with URL to landing page, click on it to test
 *    - Click "Connect" and select device, When connected the on-board LED will litted up.
 *    - Any charters received from either webusb/Serial will be echo back to webusb and Serial
 *
 * Note:
 * - The WebUSB landing page notification is currently disabled in Chrome
 * on Windows due to Chromium issue 656702 (https://crbug.com/656702). You have to
 * go to landing page (below) to test
 *
 * - On Windows 7 and prior: You need to use Zadig tool to manually bind the
 * WebUSB interface with the WinUSB driver for Chrome to access. From windows 8 and 10, this
 * is done automatically by firmware.
 *
 * - On Linux/macOS, udev permission may need to be updated by
 *     - copying '/examples/device/99-tinyusb.rules' file to /etc/udev/rules.d/ then
 *     - run 'sudo udevadm control --reload-rules && sudo udevadm trigger'
 */

#include "webusb.h"

#include <cmsis_gcc.h>
#include <device/usbd.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stm32l073xx.h>
#include <stm32l0xx.h>
#include <stm32l0xx_hal.h>
#include <stm32l0xx_hal_cortex.h>
#include <stm32l0xx_hal_def.h>
#include <stm32l0xx_hal_flash.h>
#include <stm32l0xx_hal_flash_ex.h>
#include <stm32l0xx_hal_flash_ramfunc.h>
#include <stm32l0xx_hal_rcc.h>
#include <string.h>

#include "debug.h"
#include "eeprom.h"
#include "eink.h"
#include "frame.h"
#include "sdspi.h"
#include "tusb.h"
#include "usb_descriptors.h"


// replace tu_print_mem with nothing in Release builds, because it's not defined there
#if !CFG_TUSB_DEBUG
#define tu_print_mem(...)
#endif  // CFG_TUSB_DEBUG


#define USB_BUFFER_SIZE 64  // [Byte] size of bulk buffer
#define USB_WRITE_BUFFER_SIZE (100 * USB_BUFFER_SIZE)  // [Byte] size of write_buf for images
#define USB_FW_BUFFER_SIZE (10 * FLASH_PAGE_SIZE)  // [Byte] size of write_buf for firmware updates
#define USB_MAX_OFFSET (EINK_IMAGE_BYTES)  // maximum offset for writing into an image slot
#define BOOTLOADER_SIZE 1024


const tusb_desc_webusb_url_t desc_url = {.bLength = 3 + sizeof(USB_WEBURL) - 1,
                                         .bDescriptorType = 3,  // WEBUSB URL type
                                         .bScheme = USB_WEBSCHEME,  // 0: http, 1: https
                                         .url = USB_WEBURL};

static bool web_serial_started = false;
static uint16_t usb_image_slot = 0;
static uint32_t usb_image_write_offset = 0;
static uint32_t usb_image_read_offset = 0;
static uint16_t write_buf_offset = 0;
static uint16_t read_buf_offset = 0;
static uint32_t fw_update_size = 0;
static uint32_t fw_write_flash_offset = USB_TMP_FW_START;


static void usb_apply_update(void);


bool usb_device_connected(void) {
    return web_serial_started && tud_connected();
}

//--------------------------------------------------------------------+
// Device callbacks
//--------------------------------------------------------------------+

// Invoked when device is mounted
void tud_mount_cb(void) {
    debug_print("usb mounted\n");
}

// Invoked when device is unmounted
void tud_umount_cb(void) {
    debug_print("usb unmounted\n");
    web_serial_started = false;
}

// Invoked when usb bus is suspended
// remote_wakeup_en : if host allow us    to perform remote wakeup
// Within 7ms, device must draw an average of current less than 2.5 mA from bus
void tud_suspend_cb(bool remote_wakeup_en) {
    (void)remote_wakeup_en;
}

// Invoked when usb bus is resumed
void tud_resume_cb(void) {
}

//--------------------------------------------------------------------+
// WebUSB use vendor class
//--------------------------------------------------------------------+

// Invoked when a control transfer occurred on an interface of this class
// Driver response accordingly to the request and the transfer stage (setup/data/ack)
// return false to stall control endpoint (e.g unsupported request)
bool tud_vendor_control_xfer_cb(uint8_t rhport, uint8_t stage, tusb_control_request_t const* request) {
    // return true on ack
    if (stage == CONTROL_STAGE_ACK) return true;

    // handle webusb descriptors
    if (request->bmRequestType_bit.type == TUSB_REQ_TYPE_VENDOR) {
        if (stage != CONTROL_STAGE_SETUP) return true;
        switch (request->bRequest) {
            case VENDOR_REQUEST_WEBUSB:
                // match vendor request in BOS descriptor
                // Get landing page url
                return tud_control_xfer(rhport, request, (void*)&desc_url, desc_url.bLength);

            case VENDOR_REQUEST_MICROSOFT:
                if (request->wIndex == 7) {
                    // Get Microsoft OS 2.0 compatible descriptor
                    uint16_t total_len;
                    memcpy(&total_len, desc_ms_os_20 + 8, 2);

                    return tud_control_xfer(rhport, request, (void*)desc_ms_os_20, total_len);
                } else {
                    return false;
                }

            default:
                break;
        }

        return true;
    }

    // handle class requests
    if (request->bmRequestType_bit.type == TUSB_REQ_TYPE_CLASS) {
        switch (request->bRequest) {
            case USB_CONTROL_START:
                if (request->wLength != 0) {
                    return false;
                }
                if (stage != CONTROL_STAGE_SETUP) {
                    return true;
                }
                // start data transfer mode
                usb_image_slot = 0;
                usb_image_write_offset = 0;
                usb_image_read_offset = 0;
                web_serial_started = true;
                debug_print("webusb started\n");
                return tud_control_status(rhport, request);
            case USB_CONTROL_END:
                if (request->wLength != 0) {
                    return false;
                }
                if (stage != CONTROL_STAGE_SETUP) {
                    return true;
                }
                // end data transfer mode
                web_serial_started = false;
                debug_print("webusb ended\n");
                return tud_control_status(rhport, request);
            case USB_CONTROL_REBOOT_UF2:
                // no uf2 available on st
                return false;
            case USB_CONTROL_REQUEST_FRAME_STATE: {
                static frame_info_t info = {0};
                const uint16_t offset = request->wValue;
                const uint8_t max_length = offset > sizeof(info.raw) ? 0 : sizeof(info.raw) - offset;
                if (request->bmRequestType_bit.direction == TUSB_DIR_OUT) {
                    // setting state not allowed
                    debug_print("webusb: got invalid write from host to state control\n");
                    return false;
                } else {
                    if (stage == CONTROL_STAGE_SETUP) {
                        // send current state to host
                        info = frame_get_info();
                        debug_print("webusb: sending current state");
                        return tud_control_xfer(rhport, request, info.raw + offset, max_length);
                    } else {
                        return true;
                    }
                }
            }
            case USB_CONTROL_SETTINGS: {
                static frame_settings_t settings = {0};
                const uint16_t offset = request->wValue;
                const uint8_t max_length = offset > sizeof(settings.raw) ? 0 : sizeof(settings.raw) - offset;
                if (request->bmRequestType_bit.direction == TUSB_DIR_OUT) {
                    // only accept write if usb serial connected
                    if (!web_serial_started) {
                        return false;
                    }
                    // receive data from host data stage
                    if (stage == CONTROL_STAGE_DATA) {
                        // data was set into settings, write to eeprom
                        debug_print("webusb: writing new settings\n");
                        frame_write_settings(&settings);
                        //tu_print_mem(settings.raw, sizeof(settings.raw), 0);

                        return true;
                    } else if (stage == CONTROL_STAGE_SETUP) {
                        const uint8_t length = request->wLength > max_length ? max_length : request->wLength;
                        if (length == 0) {
                            debug_print("webusb: got empty/out of bounds write request, stalling\n");
                            return false;
                        }
                        // setup array for data stage
                        // read current settings
                        frame_read_settings(&settings);

                        // get length of data
                        debug_print("webusb: receiving settings at offset %d, writing %d bytes\n", offset, length);

                        return tud_control_xfer(rhport, request, settings.raw + offset, length);
                    } else {
                        // do nothing in ack stage
                        return true;
                    }
                } else {
                    if (stage == CONTROL_STAGE_SETUP) {
                        // send data to host
                        debug_print("webusb: sending settings to host\n");
                        frame_read_settings(&settings);
                        //tu_print_mem(settings.raw, sizeof(settings.raw), 0);
                        return tud_control_xfer(rhport, request, settings.raw + offset, max_length);
                    } else {
                        // no more action needed for data and ack stage
                        return true;
                    }
                }
            }
            case USB_CONTROL_IMAGE_SLOT:
                if (request->bmRequestType_bit.direction == TUSB_DIR_OUT) {
                    // only accept in if connected properly
                    if (!web_serial_started) {
                        return false;
                    }
                    // handle data from host
                    // block invalid amount of data
                    if (request->wLength < 1 || request->wLength > 2) {
                        return false;
                    }

                    static uint16_t tmp_image_slot = 0;
                    // get image slot from host
                    if (stage == CONTROL_STAGE_SETUP) {
                        tmp_image_slot = 0;
                        return tud_control_xfer(rhport, request, &tmp_image_slot, request->wLength);
                    } else if (stage == CONTROL_STAGE_DATA) {
                        // set image slot if valid
                        if (tmp_image_slot < frame_max_images()) {
                            debug_print("webusb: set image slot to %d\n", tmp_image_slot);
                            usb_image_slot = tmp_image_slot;
                            usb_image_write_offset = 0;
                            usb_image_read_offset = 0;
                            write_buf_offset = 0;
                            read_buf_offset = 0;
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return true;
                    }
                } else {
                    // handle reading
                    // wLength needs to be two bytes
                    if (request->wLength != 2) {
                        return false;
                    }

                    // send current image slot
                    if (stage == CONTROL_STAGE_SETUP) {
                        return tud_control_xfer(rhport, request, &usb_image_slot, sizeof(usb_image_slot));
                    } else {
                        return true;
                    }
                }
            case USB_CONTROL_RESET_SETTINGS: {
                static frame_settings_t settings = {0};
                if (request->wLength != 0) {
                    return false;
                }
                if (stage != CONTROL_STAGE_SETUP) {
                    return true;
                }
                // reset settings
                debug_print("webusb: resetting settings\n");
                frame_reset_settings(&settings);
                return tud_control_status(rhport, request);
            }
            case USB_CONTROL_RECEIVE_FW:
                if (request->bmRequestType_bit.direction == TUSB_DIR_OUT) {
                    // only accept in if connected properly
                    if (!web_serial_started) {
                        return false;
                    }
                    // handle data from host
                    // block invalid amount of data
                    if (request->wLength < 1 || request->wLength > 4) {
                        return false;
                    }

                    static uint32_t tmp_fw_len = 0;
                    // get size of fw from host
                    if (stage == CONTROL_STAGE_SETUP) {
                        tmp_fw_len = 0;
                        return tud_control_xfer(rhport, request, &tmp_fw_len, request->wLength);
                    } else if (stage == CONTROL_STAGE_DATA) {
                        // enter fw update mode if sent size is valid
                        if (tmp_fw_len <= USB_MAX_FW_SIZE && tmp_fw_len > 0) {
                            fw_update_size = tmp_fw_len;
                            fw_write_flash_offset = USB_TMP_FW_START;
                            write_buf_offset = 0;

                            FLASH_EraseInitTypeDef erase = {
                                .TypeErase = FLASH_TYPEERASE_PAGES,
                                .PageAddress = FLASH_BANK2_BASE,
                                .NbPages = USB_MAX_FW_SIZE / FLASH_PAGE_SIZE
                            };
                            uint32_t page_err = 123;

                            // save and disable interrupts
                            uint32_t irqs = __get_PRIMASK();
                            __disable_irq();

                            // pre-erase second bank
                            while (HAL_FLASH_Unlock() != HAL_OK) {};
                            while (HAL_FLASHEx_Erase(&erase, &page_err) != HAL_OK);
                            HAL_FLASH_Lock();
                            HAL_FLASHEx_DATAEEPROM_Unlock();


                            // re-enable interrupts
                            __set_PRIMASK(irqs);
                            __enable_irq();

                            debug_print("flash sr: 0x%08lx\n", FLASH->SR);
                            debug_print("page error: 0x%08lx\n", page_err);

                            // stall usb if page error is not 0xffffffff
                            if (page_err == UINT32_MAX) {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return true;
                    }
                } else {
                    // this is a write only control
                    return false;
                }
        }
    }
    // stall unknown request
    return false;
}

void usb_web_task(void) {
    // reset web serial started if usb is not connected
    if (!frame_is_usb_powered() && web_serial_started) {
        web_serial_started = false;
    }

    if (web_serial_started) {
        static uint8_t buf[USB_BUFFER_SIZE] = {0};
        uint32_t count = 0;

        if (fw_update_size != 0) {
            // receive fw update

            // ensure this buffer is aligned in memory because it will be accessed as uint32_t later
            // if write_buf would be unaligned, a hardfault will occur
            static uint8_t write_buf[USB_FW_BUFFER_SIZE] __aligned(4) = {0};

            if (tud_vendor_available()) {
                count = tud_vendor_read(buf, sizeof(buf));
                if (count > 0) {
                    // track amount of bytes in write buffer
                    size_t overflow = 0;
                    if (write_buf_offset + count > sizeof(write_buf)) {
                        overflow = count - (sizeof(write_buf) - write_buf_offset);
                    }

                    // remove bytes from total count
                    fw_update_size -= fw_update_size < count ? fw_update_size : count;

                    // write received bytes into write buffer
                    size_t move_amount = count - overflow;
                    memcpy(write_buf + write_buf_offset, buf, move_amount);

                    write_buf_offset += move_amount;

                    // flush data
                    if (write_buf_offset >= sizeof(write_buf) || fw_update_size == 0) {
                        // write sector
                        uint32_t irqs = __get_PRIMASK();
                        __disable_irq();

                        while(HAL_FLASH_Unlock() != HAL_OK) {};
                        for (size_t i = 0; i < sizeof(write_buf) / sizeof(uint32_t); i++) {
                            // current address to write to
                            uint32_t addr = fw_write_flash_offset + i * sizeof(uint32_t);
                            // stop writing if end of flash was reached
                            if (addr >= FLASH_END) {
                                break;
                            }

                            // the word to write
                            uint32_t word = ((uint32_t*)write_buf)[i];
                            // write and make sure the word was actually written
                            do {
                                HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, addr, word);
                            } while (*(uint32_t*)(addr) != word);
                        }
                        HAL_FLASH_Lock();
                        HAL_FLASHEx_DATAEEPROM_Unlock();

                        __set_PRIMASK(irqs);
                        __enable_irq();

                        fw_write_flash_offset += sizeof(write_buf);
                        // reset write buffer offset
                        write_buf_offset = 0;
                        debug_print("remaining fw: %ld\n", fw_update_size);
                    }

                    // move remaining data to start of write buffer
                    if (overflow > 0) {
                        memcpy(write_buf, buf + move_amount, overflow);
                        write_buf_offset += overflow;
                    }
                }
            }

            // start moving fw
            if (fw_update_size == 0) {
                //__disable_irq();
                usb_apply_update();
            }
        } else {
            static uint8_t write_buf[USB_WRITE_BUFFER_SIZE] = {0};
            uint32_t block = 0;
            uint16_t offset = 0;
            sdspi_result_t res = SDSPI_OK;

            if (tud_vendor_available()) {
                count = tud_vendor_read(buf, sizeof(buf));
                if (count > 0) {
                    // write data into write_buf first

                    // track amount of bytes that won't fit into the write buffer anymore
                    size_t overflow = 0;
                    if (write_buf_offset + count > sizeof(write_buf)) {
                        overflow = count - (sizeof(write_buf) - write_buf_offset);
                    }

                    // write received bytes into write buffer
                    size_t move_amount = count - overflow;
                    memcpy(write_buf + write_buf_offset, buf, move_amount);

                    write_buf_offset += move_amount;

                    // flush data
                    if (write_buf_offset >= sizeof(write_buf)) {
                        // write to image data slot
                        block = (usb_image_slot * EINK_IMAGE_BYTES + usb_image_write_offset) / SDSPI_BLOCK_SIZE;
                        offset = (usb_image_slot * EINK_IMAGE_BYTES + usb_image_write_offset) % SDSPI_BLOCK_SIZE;
                        debug_print("writing to block %ld at offset %d, ", block, offset);
                        res = sdspi_write(block, offset, write_buf, sizeof(write_buf));
                        debug_print("write result: %d\n", res);
                        // advance write offset
                        if (res == SDSPI_OK) {
                            usb_image_write_offset += sizeof(write_buf);
                            usb_image_write_offset %= USB_MAX_OFFSET;
                        }

                        // reset write buffer offset
                        write_buf_offset = 0;
                    }

                    // move remaining data to start of write buffer
                    if (overflow > 0) {
                        memcpy(write_buf, buf + move_amount, overflow);
                        write_buf_offset += overflow;
                    }
                }
            }

            static uint8_t read_buf[USB_WRITE_BUFFER_SIZE] = {0};
            if (tud_vendor_write_available()) {
                // read image data
                if (read_buf_offset == 0) {
                    block = (usb_image_slot * EINK_IMAGE_BYTES + usb_image_read_offset) / SDSPI_BLOCK_SIZE;
                    offset = (usb_image_slot * EINK_IMAGE_BYTES + usb_image_read_offset) % SDSPI_BLOCK_SIZE;
                    res = sdspi_read(block, offset, read_buf, sizeof(read_buf));

                    if (res == SDSPI_OK) {
                        usb_image_read_offset += sizeof(read_buf);
                        usb_image_read_offset %= USB_MAX_OFFSET;

                        // set read offset to full buffer size
                        read_buf_offset = sizeof(read_buf);
                    }
                }

                size_t packet_size = read_buf_offset < sizeof(buf) ? read_buf_offset : sizeof(buf);
                // transmit a snippet
                count = tud_vendor_write(read_buf + sizeof(read_buf) - read_buf_offset, packet_size);
                // update remaining bytes in read buffer
                read_buf_offset -= count;
            }
        }
    }
}

// Invoked when Vendor interface received data from host
void tud_vendor_rx_cb(uint8_t itf) {
    // printf("receiving vendor on itf %d\n", itf);
}

__NO_RETURN __RAM_FUNC static void usb_apply_update(void) {
    debug_print("time to die!\n");
    __disable_irq();

    // copy over eeprom and set magic value to switch bank
    eeprom_switch_banks();

#ifdef FRAME_USE_BFB2
    // switch bank through option byte
    while(HAL_FLASH_OB_Unlock() != HAL_OK) {};
    FLASH_AdvOBProgramInitTypeDef obprog = {0};
    obprog.OptionType = OPTIONBYTE_BOOTCONFIG;
    // read option byte config
    HAL_FLASHEx_AdvOBGetConfig(&obprog);

    // toggle bank over
    obprog.OptionType = OPTIONBYTE_BOOTCONFIG;
    obprog.BootConfig = READ_BIT(SYSCFG->CFGR1, SYSCFG_CFGR1_UFB) ? OB_BOOT_BANK1 : OB_BOOT_BANK2;

    // write option byte
    HAL_FLASHEx_AdvOBProgram(&obprog);

    HAL_FLASH_OB_Launch();
#else

    // copy current bootloader into bank1 to ensure booting into bank2 works if data in bank1 is not valid
    if (READ_BIT(SYSCFG->CFGR1, SYSCFG_CFGR1_UFB)) {
        // erase 8 pages of bootloader
        while (HAL_FLASH_Unlock() != HAL_OK) {};
        FLASH_EraseInitTypeDef erase = {
            .TypeErase = FLASH_TYPEERASE_PAGES,
            .PageAddress = FLASH_BANK2_BASE,
            .NbPages = BOOTLOADER_SIZE / FLASH_PAGE_SIZE
        };
        uint32_t page_err = 123;
        while (HAL_FLASHEx_Erase(&erase, &page_err) != HAL_OK);

        // copy over bootloader from bank2 (current)
        for (size_t i = 0; i < BOOTLOADER_SIZE / sizeof(uint32_t); i++) {

            // destination address in bank2
            uint32_t addr = FLASH_BANK2_BASE + i * sizeof(uint32_t);
            // the word to write
            uint32_t word = ((uint32_t*)FLASH_BASE)[i];
            // write and make sure the word was actually written
            do {
                HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, addr, word);
            } while (*(uint32_t*)(addr) != word);
        }
    }

    // restart, bank will be switched at the start of main
    NVIC_SystemReset();
#endif  // FRAME_USE_BFB2

    while (1) {
        __NOP();
    }

    __builtin_unreachable();
}


/**
 * @brief USB interrupt handler. Calls TinyUSB interrupt handler.
 */
void USB_IRQHandler() {
    // rhport is not used, value could be anything
    tud_int_handler(BOARD_DEVICE_RHPORT_NUM);
}