/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2019 Ha Thach (tinyusb.org)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#include "usb_descriptors.h"

#include "frame.h"
#include "tusb.h"
#include "version.h"

#include <stdint.h>
#include <stdio.h>
#include <stm32l073xx.h>
#include <tusb_types.h>


//--------------------------------------------------------------------+
// Device Descriptors
//--------------------------------------------------------------------+
tusb_desc_device_t const desc_device = {
    .bLength = sizeof(tusb_desc_device_t),
    .bDescriptorType = TUSB_DESC_DEVICE,
    .bcdUSB = 0x0210,  // at least 2.1 or 3.x for BOS & webUSB

    .bDeviceClass = TUSB_CLASS_VENDOR_SPECIFIC,
    .bDeviceSubClass = 0x00,
    .bDeviceProtocol = 0x00,
    .bMaxPacketSize0 = CFG_TUD_ENDPOINT0_SIZE,

    .idVendor = USB_VID,
    .idProduct = USB_PID,
    .bcdDevice = 0x0100,

    .iManufacturer = STRING_MANUFACTURER,
    .iProduct = STRING_PRODUCT,
    .iSerialNumber = STRING_SERIAL,

    .bNumConfigurations = 0x01,
};

// Invoked when received GET DEVICE DESCRIPTOR
// Application return pointer to descriptor
uint8_t const* tud_descriptor_device_cb(void) {
    return (uint8_t const*)&desc_device;
}

//--------------------------------------------------------------------+
// Configuration Descriptor
//--------------------------------------------------------------------+
enum { ITF_NUM_VENDOR = 0, ITF_NUM_TOTAL };

#define CONFIG_TOTAL_LEN (TUD_CONFIG_DESC_LEN + TUD_VENDOR_DESC_LEN)

#if CFG_TUSB_MCU == OPT_MCU_LPC175X_6X || CFG_TUSB_MCU == OPT_MCU_LPC177X_8X || CFG_TUSB_MCU == OPT_MCU_LPC40XX
// LPC 17xx and 40xx endpoint type (bulk/interrupt/iso) are fixed by its number
// 0 control, 1 In, 2 Bulk, 3 Iso, 4 In etc ...
#define EPNUM_CDC 2
#define EPNUM_VENDOR 5
#else
#define EPNUM_CDC 2
#define EPNUM_VENDOR 3
#endif

uint8_t const desc_configuration[] = {
    // Config number, interface count, string index, total length, attribute, power in mA
    TUD_CONFIG_DESCRIPTOR(1, ITF_NUM_TOTAL, STRING_LOCALES, CONFIG_TOTAL_LEN, 0, 300),

    // Interface number, string index, EP Out & IN address, EP size
    TUD_VENDOR_DESCRIPTOR(ITF_NUM_VENDOR, STRING_VENDOR, EPNUM_VENDOR, 0x80 | EPNUM_VENDOR,
                          TUD_OPT_HIGH_SPEED ? 512 : 64),
};

// Invoked when received GET CONFIGURATION DESCRIPTOR
// Application return pointer to descriptor
// Descriptor contents must exist long enough for transfer to complete
uint8_t const* tud_descriptor_configuration_cb(uint8_t index) {
    (void)index;  // for multiple configurations
    return desc_configuration;
}

//--------------------------------------------------------------------+
// BOS Descriptor
//--------------------------------------------------------------------+

/* Microsoft OS 2.0 registry property descriptor
Per MS requirements https://msdn.microsoft.com/en-us/library/windows/hardware/hh450799(v=vs.85).aspx
device should create DeviceInterfaceGUIDs. It can be done by driver and
in case of real PnP solution device should expose MS "Microsoft OS 2.0
registry property descriptor". Such descriptor can insert any record
into Windows registry per device/configuration/interface. In our case it
will insert "DeviceInterfaceGUIDs" multistring property.

GUID is freshly generated and should be OK to use.

https://developers.google.com/web/fundamentals/native-hardware/build-for-webusb/
(Section Microsoft OS compatibility descriptors)
*/

#define BOS_TOTAL_LEN (TUD_BOS_DESC_LEN + TUD_BOS_WEBUSB_DESC_LEN + TUD_BOS_MICROSOFT_OS_DESC_LEN)

#define MS_OS_20_DESC_LEN 0x9E  // Full length of the descriptor, contains SET_HEADER, COMPAT_ID and REG_PROP
#define MS_OS_20_SET_HEADER_LEN 0x0A  // Length of Set Header descriptor
#define MS_OS_20_COMPAT_ID_LEN 0x14  // Length of Compatible ID descriptor
#define MS_OS_20_REG_PROP_LEN 0x80  // Full Registry Property descriptor, contains PROP_NAME and PROP_DATA and wPropertyDataType
#define MS_OS_20_PROP_NAME_LEN 0x28  // Length of property name
#define MS_OS_20_PROP_DATA_LEN 0x4E  // Length of property data

// BOS Descriptor is required for webUSB
uint8_t const desc_bos[] = {
    // total length, number of device caps
    TUD_BOS_DESCRIPTOR(BOS_TOTAL_LEN, 2),

    // Vendor Code, iLandingPage
    TUD_BOS_WEBUSB_DESCRIPTOR(VENDOR_REQUEST_WEBUSB, 1),

    // Microsoft OS 2.0 descriptor
    TUD_BOS_MS_OS_20_DESCRIPTOR(MS_OS_20_DESC_LEN, VENDOR_REQUEST_MICROSOFT),
};

uint8_t const* tud_descriptor_bos_cb(void) {
    return desc_bos;
}


uint8_t const desc_ms_os_20[] = {
    // Set header: length, type, windows version, total length
    U16_TO_U8S_LE(MS_OS_20_SET_HEADER_LEN), U16_TO_U8S_LE(MS_OS_20_SET_HEADER_DESCRIPTOR), U32_TO_U8S_LE(0x06030000),
    U16_TO_U8S_LE(MS_OS_20_DESC_LEN),

    // Configuration Subset and Function Subset header must not be included because only one interface exists
    // Immediately continue with the Compatible ID descriptor and Registry Property descriptor

    // MS OS 2.0 Compatible ID descriptor: length, type, compatible ID, sub compatible ID
    U16_TO_U8S_LE(MS_OS_20_COMPAT_ID_LEN), U16_TO_U8S_LE(MS_OS_20_FEATURE_COMPATBLE_ID), 'W', 'I', 'N', 'U', 'S', 'B', '\0', 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // sub-compatible

    // MS OS 2.0 Registry property descriptor: length, type
    U16_TO_U8S_LE(MS_OS_20_REG_PROP_LEN), U16_TO_U8S_LE(MS_OS_20_FEATURE_REG_PROPERTY),
    U16_TO_U8S_LE(0x0007),  // wPropertyDataType, here 7 == REG_MULTI_SZ
    U16_TO_U8S_LE(MS_OS_20_PROP_NAME_LEN),  // wPropertyNameLength and PropertyName "DeviceInterfaceGUID\0" in UTF-16
    // Only one interface exists, so only one GUID needed
    // If more interfaces exist, then it needs to be "DeviceInterfaceGUIDs\0" and a list of GUIDs + additional NULL terminator
    'D', 0x00, 'e', 0x00, 'v', 0x00, 'i', 0x00, 'c', 0x00, 'e', 0x00, 'I', 0x00, 'n', 0x00, 't', 0x00, 'e', 0x00, 'r',
    0x00, 'f', 0x00, 'a', 0x00, 'c', 0x00, 'e', 0x00, 'G', 0x00, 'U', 0x00, 'I', 0x00, 'D', 0x00, 0x00, 0x00,
    U16_TO_U8S_LE(MS_OS_20_PROP_DATA_LEN),  // wPropertyDataLength
    // bPropertyData: “{45C8EB55-30E6-4F01-AF7E-453A7011059B}”.
    '{', 0x00,
    '4', 0x00, '5', 0x00, 'C', 0x00, '8', 0x00, 'E', 0x00, 'B', 0x00, '5', 0x00, '5', 0x00, '-', 0x00,
    '3', 0x00, '0', 0x00, 'E', 0x00, '6', 0x00, '-', 0x00,
    '4', 0x00, 'F', 0x00, '0', 0x00, '1', 0x00, '-', 0x00,
    'A', 0x00, 'F', 0x00, '7', 0x00, 'E', 0x00, '-', 0x00,
    '4', 0x00, '5', 0x00, '3', 0x00, 'A', 0x00, '7', 0x00, '0', 0x00, '1', 0x00, '1', 0x00, '0', 0x00, '5', 0x00, '9', 0x00, 'B', 0x00,
    '}', 0x00,
    0x00, 0x00,  // null terminator
};

TU_VERIFY_STATIC(sizeof(desc_ms_os_20) == MS_OS_20_DESC_LEN, "Incorrect size");

//--------------------------------------------------------------------+
// String Descriptors
//--------------------------------------------------------------------+
// buffer to store chip id string
static char chip_id_str[FRAME_UID_BYTES * 2 + 1];
static bool chip_id_set = false;
static char product_str[12 + 16 + 1] = "picoframe - ";
static bool product_str_set = false;
// array of pointer to string descriptors
char const* string_desc_arr[STRINGS_AMOUNT] = {
    (const char[]){0x09, 0x04},  // 0: is supported language is English (0x0409)
    "picoframe",  // 1: Manufacturer
    product_str,  // 2: Product
    chip_id_str,  // 3: Serials, should use chip ID
    "picoframe WebUSB",  // 4: Vendor Interface
};

static uint16_t _desc_str[32];

// Invoked when received GET STRING DESCRIPTOR request
// Application return pointer to descriptor, whose contents must exist long enough for transfer to complete
uint16_t const* tud_descriptor_string_cb(uint8_t index, uint16_t langid) {
    (void)langid;
    static const uint32_t* const uid1 = (void*)UID_BASE;
    static const uint32_t* const uid2 = (void*)UID_BASE + 0x14;

    // get chip id string on first call
    if (!chip_id_set) {
        snprintf(chip_id_str, sizeof(chip_id_str), "%08lx%08lx%08lx", uid1[0], uid1[1], uid2[0]);

        const version_info_t *const version_info = version_get_info();
        // also set version strings
        string_desc_arr[STRING_VERSION] = version_get_str();
        string_desc_arr[STRING_COMMIT] = version_get_commit();
        string_desc_arr[STRING_BUILD_CONFIG] = version_get_build_config();
        string_desc_arr[STRING_BUILD_DATE] = version_info->date;
        string_desc_arr[STRING_BUILD_TIME] = version_info->time;

        chip_id_set = true;
    }

    // get product string
    if (!product_str_set) {
        frame_device_name_t name = {0};
        frame_get_device_name(&name);
        memcpy(product_str + 12, name, FRAME_DEVICE_NAME_LENGTH);
        product_str[sizeof(product_str) - 1] = 0;
    }

    uint8_t chr_count;

    if (index == 0) {
        memcpy(&_desc_str[1], string_desc_arr[0], 2);
        chr_count = 1;
    } else {
        const char* str;

        // https://docs.microsoft.com/en-us/windows-hardware/drivers/usbcon/microsoft-defined-usb-descriptors
        // Note: the 0xEE index string is a Microsoft OS 1.0 Descriptors.
        // Not used at the moment, implementation not tested
        // Windows (at least Win11) doesn't try to read it anyway, probably bacause the 2.0 descriptors are read first??
        // The 1.0 descriptors might be useful for Win7 and Vista support, but idk
        // Another reference: https://github.com/pbatard/libwdi/wiki/WCID-Devices#microsoft-os-string-descriptor=
        if (index == 0xEE) {
#ifdef MS_OS_10_DESC
            const uint16_t sig[] = u"MSFT100!";
            tusb_desc_string_t* ms_desc = (void*)_desc_str;
            ms_desc->bLength = sizeof(sig) + sizeof(tusb_desc_string_t);
            ms_desc->bDescriptorType = TUSB_DESC_STRING;

            // put signature into string
            for (size_t i = 0; i < sizeof(sig) / sizeof(uint16_t); i++) {
                ms_desc->unicode_string[i] = sig[i];
            }
            return _desc_str;
#else
            return NULL;
#endif  // MS_OS_10_DESC

        } else if (!(index < sizeof(string_desc_arr) / sizeof(string_desc_arr[0]))) {
            return NULL;
        }
        str = string_desc_arr[index];

        // Cap at max char
        chr_count = strlen(str);
        if (chr_count > 31) chr_count = 31;

        // Convert ASCII string into UTF-16
        for (uint8_t i = 0; i < chr_count; i++) {
            _desc_str[1 + i] = str[i];
        }
    }

    // first byte is length (including header), second byte is string type
    _desc_str[0] = (TUSB_DESC_STRING << 8) | (2 * chr_count + 2);

    return _desc_str;
}
