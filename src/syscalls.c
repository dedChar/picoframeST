#include "syscalls.h"

#include "usart.h"
#include <stm32l0xx_hal_def.h>
#include <stm32l0xx_hal_uart.h>


/**
 * @brief   Write syscall to redirect printf() to UART.
 * @param   fd File descriptor, unused.
 * @param   ptr Buffer of bytes to write.
 * @param   len Amount of bytes to write.
 * @return  Number of bytes written
 */
int _write(int fd, char* ptr, int len) {
    UNUSED(fd);

    HAL_UART_Transmit(&hlpuart1, (uint8_t*)ptr, len, HAL_MAX_DELAY);
    return len;
}