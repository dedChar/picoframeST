# picoframeST
A digital picture frame using a [7-color E-Paper display](https://www.waveshare.com/5.65inch-e-paper-f.htm), configurable via WebUSB.

This repo contains the software running on the device and the schematics for the hardware. An interactive BOM for the hardware can be found [here](https://bom.picofra.me/).

## Features
- Display 600x448px large images with up to 7 distinct colors
- Upload pictures and configuration via USB
- Battery powered with charging via the USB-C port
- Low power consumtion of ~8µA when picture is not changing
- Configurable timer to change displayed picture
- Display pictures either in a specific order or shuffled
- Buttons on the back to manually switch to the next/previous picture
- Configuration via WebUSB on https://picofra.me/ (still WIP)

## Pictures
### Front with display
![Front](https://static.beal2o4.moe/picoframe/front.jpg)
### Back
![Back](https://static.beal2o4.moe/picoframe/back.jpg)
### Picture change (Video, click on it)
[![](http://img.youtube.com/vi/UP-pVkwXiI0/0.jpg)](http://www.youtube.com/watch?v=UP-pVkwXiI0 "picoframe changing dsiplayed picture")

## License
See [LICENSE](https://gitlab.com/dedChar/picoframeST/-/blob/main/LICENSE)
