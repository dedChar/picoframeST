#!/bin/env python3

# pad a file to a desired size with 0xff

import sys

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("usage: pad.py <input file> <output file> <padded size>", file=sys.stderr)
        sys.exit(1)

    try:
        goal = int(sys.argv[3])
    except ValueError:
        try:
            goal = int(sys.argv[3], 16)
        except ValueError:
            print(f"error: '{sys.argv[3]}' is not a number", file=sys.stderr)
            sys.exit(1)

    in_f = open(sys.argv[1], "rb")
    out_f = open(sys.argv[2], "wb")

    out_f.write(in_f.read())

    missing = max(goal - in_f.tell(), 0)
    out_f.write(bytes([0xff] * missing))

    in_f.close()
    out_f.close()

