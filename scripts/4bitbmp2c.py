import sys
import os
import struct
from math import ceil

# size of standard bmp header
HEADER_SIZE = 14
# size of legacy info header
INFOHEADER_SIZE_SMALL = 12

# byte position of pixel array address in bmp header
PIXEL_ARRAY_POINTER_POSITION = 10

# 7-color display uses 4 (3) bits per pixel to encode color
ACEP_PIXEL_DEPTH = 4


if __name__ == "__main__":
    # need filepath as arg
    if len(sys.argv) < 2:
        print("missing filepath!", file=sys.stderr)
        exit(-1)

    # get optional bitmap name
    name = None
    if len(sys.argv) > 2:
        name = sys.argv[2].upper()

    raw = "--raw" in sys.argv
    bw = "--black-white" in sys.argv

    # get file
    filepath = os.path.realpath(sys.argv[1])
    folder, filename = os.path.split(filepath)

    pixels = None
    width, height = 0, 0
    # open bmp file
    with open(filepath, mode="rb") as file:
        # read BMP header
        header = file.read(HEADER_SIZE)
        # check BM header
        if header[:2] != b"BM":
            print("bad header in BMP", file=sys.stderr)
            exit(-1)

        # get pixel array start
        arrstart, = struct.unpack("<I", header[PIXEL_ARRAY_POINTER_POSITION:])

        # get size of extended header
        infosize, = struct.unpack("<I", file.read(4))

        # get width and height
        if infosize == INFOHEADER_SIZE_SMALL:
            # old header, width and height are uint16
            width, height = struct.unpack("<HH", file.read(4))
        else:
            # newer header, width and height are int32
            width, height = struct.unpack("<ii", file.read(8))

        print(f"got bitmap dimentions {width}x{height}", file=sys.stderr)
        # get color info
        color_panes, color_depth = struct.unpack("<HH", file.read(4))

        # abort if color depth doesn't match
        if color_depth != ACEP_PIXEL_DEPTH:
            print(f"BMP has a color depth of {color_depth}, expected {ACEP_PIXEL_DEPTH}!", file=sys.stderr)
            exit(-1)

        # go to start of pixel array
        file.seek(arrstart)
        # read all bytes
        pixels = file.read()

    num_bytes = len(pixels)
    row_size = ceil((ACEP_PIXEL_DEPTH * width) / 32) * 4
    print(f"width: {width}, row_size: {row_size}")

    # encode color as bits
    if bw:
        num_bytes = num_bytes // 4 + int((num_bytes % 4 != 0))
        #num_bytes = ((width + width % 8) // 8) * height

    # print out formatted c array
    if not raw:
        if name is None:
            print("#include <stdint.h>")
            print("")
            print(f"#define BITMAP_SIZE {num_bytes}")
            print("")
            print(f"const uint8_t BITMAP[BITMAP_SIZE] = " + "{")
        else:
            print(f"#define BITMAPS_{name}_SIZE {num_bytes}")
            print("")
            print(f"const uint8_t BITMAP_{name}[BITMAPS_{name}_SIZE] = " + "{")

    if not bw:
        for y in range(height):
            # start new row
            if not raw:
                print("    ", end="")
            # get a slice
            row = pixels[len(pixels) - ((y + 1) * (width // 2)):len(pixels) - (y * (width // 2))]
            for x, b in enumerate(row):
                #num = struct.unpack("<B", b)
                if not raw:
                    print(f"{b:#04x},", end="")
                    if x != (width // 2) - 1:
                        print(" ", end="")
                else:
                    sys.stdout.buffer.write(b.to_bytes(1, byteorder="little"))
    else:
        # expand list of pixels for easier processing of uneven widths
        expanded = []
        if width % 8 == 0:
            padded_width = width
        else:
            padded_width = width + 8 - width % 8  # number of pixels per row with additional padding pixels

        for y in range(height):
            row = pixels[len(pixels) - (y + 1) * row_size:len(pixels) - (y) * row_size]
            for x in range(ceil(width / 2)):
                b = row[x]
                expanded.append((b >> 4) & 0x0f)
                # use only half a byte at the end of an uneven width
                if width % 2 and x == ceil(width / 2) - 1:
                    break
                expanded.append(b & 0x0f)
            # add additional padding pixels to pad out the rows
            for _ in range(padded_width - width):
                expanded.append(0)

        if not raw:
            print("    ", end="")
        byte_count = 1
        bitmap = []
        for y in range(height):
            # get one row of pixels, starting from the end -> rows are stored backwards
            #row = expanded[len(expanded) - ((y + 1) * padded_width):len(expanded) - (y * padded_width)]
            #row = expanded[y * padded_width:(y+1) * padded_width]

            b = 0  # current bitmap byte
            pxl = 0  # current pixel in bitmap byte
            for x in range(padded_width):
                p = expanded[x + y * padded_width]
                # set bit if pixel is black
                b |= (1 << (pxl)) if p == 0 else 0

                pxl += 1  # got one pixel done
                if pxl >= 8:
                    # write byte into bitmap row
                    bitmap.append(b)
                    # reset values
                    pxl = 0
                    b = 0

        # print the bitmap row
        for i, b in enumerate(bitmap):
            if not raw:
                """
                x, y = (i * 8) % padded_width, (i * 8) // padded_width
                if x == 0:
                    print("")
                for pxl in range(8):
                    print(f"{'■' if b >> pxl & 1 else '□'}", end="")
                    x += 1
                    if x >= width:
                        break
                """
                print(f"{b:#04x}, ", end="")
                #print(len(bitmap))

                if (byte_count) % 16 == 0 and byte_count != 0:
                    print("")
                    print("    ", end="")
                    # reset back to 0
                    byte_count = 0
                # increment byte count to keep track of whent to add a newline
                byte_count += 1
            else:
                sys.stdout.buffer.write(b.to_bytes(1, byteorder="little"))


        # add newline
        if not raw:
            print("")

    # add closing brace
    if not raw:
        print("};\n")
