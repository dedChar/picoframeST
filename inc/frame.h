#ifndef FRAME_H
#define FRAME_H


#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stm32l0xx_hal_gpio.h>
#include <stm32l0xx_hal_pwr.h>
#include <stm32l0xx_hal_rtc.h>


#include "eink.h"
#include "sdspi.h"

#define FRAME_EINK_SPI &hspi1  // SPI interface to use for eink display
#define FRAME_CARD_SPI &hspi2  // SPI interface to use for card

#define FRAME_RTC_ALARM (RTC_ALARM_A)  // RTC alarm used for periodic wakeups
#define FRAME_RTC_ALARM_FLAG (RTC_FLAG_ALRAF)
#define FRAME_VUSB_WAKE_PIN (PWR_WAKEUP_PIN1)
#define FRAME_BTN_WAKE_PIN (PWR_WAKEUP_PIN2)

#define FRAME_DEVICE_NAME_LENGTH 16  // [Byte] length of custom device name
#define FRAME_SETTINGS_ADDRESS 0  // address inside eeprom to find settings
#define FRAME_RANDOM_SEED_ADDRESS (FRAME_SETTINGS_ADDRESS + sizeof(frame_settings_t) + 0)
#define FRAME_DISPLAY_CONTENT_ADDRESS (FRAME_RANDOM_SEED_ADDRESS + sizeof(uint32_t) + 0)
#define FRAME_SHUFFLE_TABLE_ADDRESS (FRAME_DISPLAY_CONTENT_ADDRESS + sizeof(uint32_t) + 0)
#define FRAME_SHUFFLE_TABLE2_ADDRESS (FRAME_SHUFFLE_TABLE_ADDRESS + FRAME_SHUFFLE_TABLE_MAX_SIZE + 0)
#define FRAME_SHUFFLE_TABLE_MAX_SIZE 16

#define FRAME_PERIPH_ON GPIO_PIN_SET  // PERIPH_EN is active low
#define FRAME_PERIPH_OFF GPIO_PIN_RESET

#define FRAME_UID_WORDS 3  // number of words in uid registers
#define FRAME_UID_BYTES (FRAME_UID_WORDS * sizeof(uint32_t))

// number of slots available in frame_bitmaps buffer
#define FRAME_BITMAP_BUFFER_SIZE 5
// base scale to use when scale is set to 1, set to 1px/part -> 1px bitmap == 4px screen
#define FRAME_BITMAP_BASE_SCALE_Y (EINK_LINES_PER_PART)
// base scale to use when scale is set to one, set to same as SCALE_Y. Minimum is 2 because 1 would require working with half bytes.
#define FRAME_BITMAP_BASE_SCALE_X (FRAME_BITMAP_BASE_SCALE_Y / 2)


// coordinates for bitmaps
#define FRAME_CARD_ICON_COORDS 172, 50
#define FRAME_USB_ICON_COORDS 128, 31
#define FRAME_PHOTO_ICON_COORDS 140, 34
#define FRAME_TEXT_COORDS 172, 386


typedef char frame_device_name_t[FRAME_DEVICE_NAME_LENGTH];


typedef enum {
    FRAME_STATE_NONE = 0xff,
    FRAME_STATE_INIT = 0,
    FRAME_STATE_IDLE,
    FRAME_STATE_SETUP,
    FRAME_STATE_NO_IMAGES,
    FRAME_STATE_DISPLAY,
    FRAME_STATE_WAITING,
    FRAME_STATE_WEBUSB_CONNECTED,
    FRAME_STATE_GO_SLEEP,
    FRAME_STATE_ERROR,
    FRAME_STATE_UPDATE
} __packed frame_state_t;

typedef enum {
    FRAME_SETTINGS_NONE = 0,
    FRAME_SETTINGS_V1,
    FRAME_SETTINGS_UNSUPPORTED,
} __packed frame_settings_version_t;


typedef union {
    struct {
        uint8_t shuffle : 1;
        uint8_t name_set : 1;
        uint8_t setup_completed : 1;
        uint8_t buttons_disabled : 1;
        uint8_t led_disabled : 1;
        uint8_t : 3;
    } bits;
    uint8_t raw;
} __packed frame_flags_t;
static_assert(sizeof(frame_flags_t) == 1, "Frame flags need to use exactly 1 byte.");

typedef struct {
    uint8_t minutes;
    uint8_t hours;
} __packed frame_timer_t;
static_assert(sizeof(frame_timer_t) == 2, "Frame timer needs to use exactly 2 bytes.");

typedef union {
    struct {
        frame_settings_version_t version;  // Settings version
        uint8_t card_cid[SDSPI_CID_CSD_SIZE];  // CID of last setup card
        frame_device_name_t device_name;  // Custom device name
        frame_flags_t flags;  // settings flags
        uint16_t num_images;  // number of loaded images
        frame_timer_t timer;  // timer when to display the next image
        frame_timer_t timer_remaining;  // remaining time until next image will be displayed
        uint16_t last_image;  // index of last displayed image, to not display the last image twice on shuffle
        uint16_t crc16;  // CRC16 checksum of all settings
    } __packed fields;
    uint8_t raw[sizeof(frame_settings_version_t) + SDSPI_CID_CSD_SIZE + FRAME_DEVICE_NAME_LENGTH +
                sizeof(frame_flags_t) + 3 * sizeof(uint16_t) + 2 * sizeof(frame_timer_t)];
    uint8_t data[sizeof(frame_settings_version_t) + SDSPI_CID_CSD_SIZE + FRAME_DEVICE_NAME_LENGTH +
                 sizeof(frame_flags_t) + 2 * sizeof(uint16_t) + 2 * sizeof(frame_timer_t)];  // array without crc
} frame_settings_t;
static_assert(sizeof(frame_settings_t) == 44, "Frame settings need to be exactly 44 bytes.");

typedef union {
    struct {
        frame_state_t state;
        bool is_dev : 1;
        bool is_bank2 : 1;
        bool card_inserted : 1;
        uint8_t : 5;
        uint16_t max_images;
        sdspi_result_t last_card_error;
        sdspi_card_t card_type;
    } __packed;
    uint8_t raw[sizeof(frame_state_t) + sizeof(uint8_t) + sizeof(uint16_t) + sizeof(sdspi_result_t) + sizeof(sdspi_card_t)];
} __packed frame_info_t;
static_assert(sizeof(frame_info_t) == 6, "Frame info needs to be exactly 6 bytes");


void frame_init();
void frame_task();
void frame_get_device_name(frame_device_name_t* dest);
void frame_read_settings(frame_settings_t* settings);
void frame_write_settings(frame_settings_t* settings);
void frame_reset_settings(frame_settings_t* settings);
frame_info_t frame_get_info();
void frame_enter_sleep(const bool wakeup_alarm, const bool wakeup_usb, const bool wakeup_buttons);
void frame_setup_alarm(const frame_timer_t timer);


static inline bool frame_is_usb_powered() {
    return HAL_GPIO_ReadPin(VUSB_SENSE_GPIO_Port, VUSB_SENSE_Pin);
}

static inline uint16_t frame_max_images() {
    // assumes 300*448 bytes per image and 512 byte block size
    static_assert(EINK_IMAGE_BYTES == 300 * 448, "unexpected number of bytes in image");
    static_assert(SDSPI_BLOCK_SIZE == 512, "unexpected card block size");
    uint32_t max_images = sdspi_capacity_blocks() / ((EINK_IMAGE_BYTES * 2) / SDSPI_BLOCK_SIZE);
    // add an additional image, if it would fit into the last blocks
    const uint16_t remaining = sdspi_capacity_blocks() % ((EINK_IMAGE_BYTES * 2) / SDSPI_BLOCK_SIZE);
    bool odd = false;
    if (remaining > EINK_IMAGE_BYTES / SDSPI_BLOCK_SIZE + (EINK_IMAGE_BYTES % SDSPI_BLOCK_SIZE != 0)) {
        odd = true;
    }

    return max_images * 2 + odd > UINT16_MAX ? UINT16_MAX : max_images;
}


#endif  // FRAME_H