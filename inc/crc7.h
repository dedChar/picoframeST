#ifndef CRC7_H
#define CRC7_H

// based on https://github.com/hazelnusse/crc7

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


#define CRC7_POLY 0x89


uint8_t crc7_get(const uint8_t* data, const size_t length);
bool crc7_verify(const uint8_t crc, const uint8_t* data, const size_t length);


#endif  // CRC7_H