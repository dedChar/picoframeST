#ifndef EINK_H
#define EINK_H


#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "spi.h"


#define EINK_SPI hspi1
#define EINK_SPI_BAUD 2000000
#define EINK_SPI_TIMEOUT 10  // [ms] timout for blocking spi
#define EINK_HEIGHT 448
#define EINK_WIDTH 600
#define EINK_NUM_PIXELS (EINK_WIDTH * EINK_HEIGHT)  // number of pixels in one full pixels
#define EINK_IMAGE_BYTES (EINK_NUM_PIXELS / 2)  // number of bytes needed to store one full image
#define EINK_BUFFER_PARTS 112  // number of parts to split the screen buffer into
#define EINK_LINES_PER_PART (EINK_HEIGHT / EINK_BUFFER_PARTS)
#define EINK_BUFFER_SIZE (EINK_IMAGE_BYTES / EINK_BUFFER_PARTS)  // size of the image buffer used to hold part of the image to display
#define EINK_RESET_HOLD_TIME 1  // [ms]
#define EINK_SCREEN_UPDATE_DELAY 200  // [ms] to wait after sending last command of a display update/clean sequence
#define EINK_SLEEP_DELAY 100  // [ms] to wait in sleep function

// transmission modes
#define EINK_DATA 1
#define EINK_COMMAND 0

// fill one byte with a color
#define EINK_COLOR_BYTE(col) ((col) | (col) << 4)
// get the buffer index for pixel x, y coords
#define EINK_PIXEL_IDX(width, x, y) ((y) * ((width) / 2) + ((x) / 2))
#define EINK_BUFFER_PIXEL_IDX(x, y) ((y) * (EINK_WIDTH / 2) + ((x) / 2))
// shift a value to correct nibble in byte
#define EINK_SHIFT_TO_PIXEL(val, x) ((val) << (4 * ((x) % 2 == 0)))
// set a pixel to a color in buffer
#define EINK_BUFFER_SET(buf, x, y, col) \
    do { \
        (buf)[EINK_BUFFER_PIXEL_IDX((x), (y))] &= ~EINK_SHIFT_TO_PIXEL(0x0F, (x)); \
        (buf)[EINK_BUFFER_PIXEL_IDX((x), (y))] |= EINK_SHIFT_TO_PIXEL((col), (x)); \
    } while (0);
// get the value of a pixel from buffer
#define EINK_PIXEL_GET(buf, width, x, y) (((buf)[EINK_PIXEL_IDX((width), (x), (y))] >> (4 * ((x) % 2 == 0))) & 0x7)
#define EINK_BUFFER_GET(buf, x, y) (EINK_PIXEL_GET(buf, EINK_WIDTH, x, y))

#define EINK_COMMAND_LIST(X) \
    X(PANEL_SETTING, 0x00) \
    X(POWER_SETTING, 0x01) \
    X(POWER_OFF, 0x02) \
    X(POWER_OFF_SEQ, 0x03) \
    X(POWER_ON, 0x04) \
    X(BOOSTER_SOFT_START, 0x06) \
    X(DEEPSLEEP, 0x07) \
    X(DATA_START, 0x10) \
    X(DATA_STOP, 0x11) \
    X(DISPLAY_REFRESH, 0x12) \
    X(PLL_CONTROL, 0x30) \
    X(TEMP_SENSOR_ENABLE, 0x41) \
    X(VCOM_SETTING, 0x50) \
    X(TCON, 0x60) \
    X(TRES, 0x61) \
    X(POWERSAVE, 0xe3)

#define EINK_COMMAND_ENUM_EXPANSION(name, value) EINK_COMMAND_##name = value,


typedef enum { EINK_COMMAND_LIST(EINK_COMMAND_ENUM_EXPANSION) } eink_command_t;

typedef enum {
    EINK_COLOR_BLACK = 0,
    EINK_COLOR_WHITE,
    EINK_COLOR_GREEN,
    EINK_COLOR_BLUE,
    EINK_COLOR_RED,
    EINK_COLOR_YELLOW,
    EINK_COLOR_ORANGE,
    EINK_COLOR_CLEAN,
} eink_color_t;


// typedef of eink buffer array type
typedef uint8_t eink_buffer_t[EINK_BUFFER_SIZE];
// callback function type for the update callback
typedef void (*eink_update_cb_t)();
// callback function type for preparing the next slice of data
typedef void (*eink_prep_data_cb_t)(const size_t part, eink_buffer_t* buffer);


void eink_init(SPI_HandleTypeDef* spi);
void eink_reset();
void eink_set_pixel(const size_t x, const size_t y, const eink_color_t color);
void eink_draw_bitmap(const uint8_t* bitmap, const size_t x, const size_t y, const size_t w, const size_t h);
void eink_fill_color(const eink_color_t color);
void eink_clean(eink_update_cb_t callback);
void eink_update(eink_update_cb_t update_cb, eink_prep_data_cb_t prep_cb);
void eink_sleep();
void eink_trigger_reset();
eink_buffer_t* eink_get_buffer();
bool eink_is_updating();
void eink_task();

// BUSY GPIO interrupt handler
void EXTI0_1_IRQHandler(void);


#endif  // EINK_H