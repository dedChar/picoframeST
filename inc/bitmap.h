#ifndef BITMAPS_H
#define BITMAPS_H


#include "eink.h"
#include <stddef.h>
#include <stdint.h>


// list of all bitmaps containing name, width and height
#define BITMAP_LIST(X) \
    X(QRCODE, 25, 25) \
    X(NO_IMAGE, 53, 7) \
    X(CARD_ERROR, 59, 7) \
    X(SCAN_AND_SETUP, 75, 8)


// macro to setup the bitmap struct data for one bitmap
#define BITMAP_PREP_STRUCT(ptr, bitmap_name) \
do { \
    (ptr)->bitmap = BITMAP_##bitmap_name; \
    (ptr)->size = BITMAP_##bitmap_name##_SIZE; \
    (ptr)->pixel_height = BITMAP_##bitmap_name##_HEIGHT; \
    (ptr)->pixel_width = BITMAP_##bitmap_name##_WIDTH; \
} while(0);

// expansion macro to expand width enum
#define BITMAP_EXPAND_WIDTH_ENUM(name, width, height) BITMAP_##name##_WIDTH = width,
// expansion macro to expand height enum
#define BITMAP_EXPAND_HEIGHT_ENUM(name, width, height) BITMAP_##name##_HEIGHT = height,
// expansion macro to expand size enum
#define BITMAP_EXPAND_SIZE_ENUM(name, width, height) BITMAP_##name##_SIZE = ((((width + (8 - (width % 8)) * (width % 8 != 0)) / 8) * height)),
// expansion macro to declare the bitmaps as extern arrays
#define BITMAP_EXPAND_EXTERN_DECLARATION(name, width, height) \
    extern const uint8_t BITMAP_##name[BITMAP_##name##_SIZE];


typedef struct {
    const uint8_t* bitmap;
    size_t size;
    size_t pixel_width;
    size_t pixel_height;
    eink_color_t background;
    eink_color_t foreground;
    size_t x;
    size_t y;
    size_t scale;
} bitmap_render_t;


typedef enum { BITMAP_LIST(BITMAP_EXPAND_WIDTH_ENUM) } bitmap_width_t;

typedef enum { BITMAP_LIST(BITMAP_EXPAND_HEIGHT_ENUM) } bitmap_height_t;

typedef enum { BITMAP_LIST(BITMAP_EXPAND_SIZE_ENUM) } bitmap_size_t;


BITMAP_LIST(BITMAP_EXPAND_EXTERN_DECLARATION)


#endif  // BITMAPS_H