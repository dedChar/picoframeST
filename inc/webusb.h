#ifndef WEBUSB_H
#define WEBUSB_H

#include <stdbool.h>
#include <stdint.h>

#include "tusb.h"


#define USB_WEBURL "picofra.me"  // landing page of webusb device
#define USB_WEBSCHEME 1  // 0: http, 1: https

#define USB_MAX_FW_SIZE (FLASH_SIZE >> 1)  // [Byte] 64KiB
#define USB_TMP_FW_START (FLASH_BASE + 64 * 1024)  // [Byte] start offset fot storing new fw data in flash


// control request types
typedef enum {
    USB_CONTROL_REQUEST_FRAME_STATE = 1,
    USB_CONTROL_SETTINGS,
    USB_CONTROL_IMAGE_SLOT,
    USB_CONTROL_RESET_SETTINGS,
    USB_CONTROL_START = 0x22,
    USB_CONTROL_END = 0x44,
    USB_CONTROL_REBOOT_UF2 = 0x66,
    USB_CONTROL_RECEIVE_FW = 0x88,
} usb_control_request_t;


void usb_cdc_task(void);
void usb_web_task(void);
bool usb_device_connected(void);


// tinyusb callbacks
void tud_mount_cb(void);
void tud_umount_cb(void);
void tud_suspend_cb(bool remote_wakeup_en);
void tud_resume_cb(void);
bool tud_vendor_control_xfer_cb(uint8_t rhport, uint8_t stage, tusb_control_request_t const* request);
void tud_cdc_line_state_cb(uint8_t itf, bool dtr, bool rts);
void tud_cdc_rx_cb(uint8_t itf);
void tud_vendor_rx_cb(uint8_t itf);


#endif  // WEBUSB_H