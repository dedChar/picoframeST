#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>

#ifndef STUB_PRINTF
#include "syscalls.h"
#define debug_print(...) printf(__VA_ARGS__)
#else
#define debug_print(...)
#endif  // STUB_PRINTF

#endif  // DEBUG_H