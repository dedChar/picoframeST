#ifndef SDSPI_H
#define SDSPI_H


#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <stm32l0xx_hal_spi.h>


#define SDSPI_CARD_DETECTION_PIN_ENABLED

// first bit of transmission is 0 (start bit), second is 1 (transmission bit)
#define SDSPI_COMMAND_PRELUDE 0x40
// bit mask to mask out command number
#define SDSPI_COMMAND_BITS_MASK 0x3f
// bit mask to mask out 0 bits from data response packets
#define SDSPI_DATA_RESPONSE_BITS_MASK 0x0f
// [Byte] block size to use in bytes. SDHC+ cards are fixed to 512.
#define SDSPI_BLOCK_SIZE 512
// [Byte] size of CID/CSD data in data frame
#define SDSPI_CID_CSD_SIZE 16
// use 100kHz clock for initial baufrate in init sequence
#define SDSPI_INIT_BAUDRATE_SCALE SPI_BAUDRATEPRESCALER_128
#define SDSPI_FULL_BAUDRATE_SCALE SPI_BAUDRATEPRESCALER_2
// CMD8 argument used when initializing
#define SDSPI_IF_COND 0x1AAu
// HSC bit used in argument of ACMD41 to signal high capacity support
#define SDSPI_HSC_BIT (1 << 30)
// CCS bit in OCR (CMD58), CCS=1 indicates SDHC/XC, CCS=0 indicates SDSC
#define SDSPI_CCS_BIT (1 << 30)
// data to transmit when reading from sd
#define SDSPI_TX_DATA 0xff
// maximum number of tries to use for resending failed packages
#define SDSPI_MAX_RETRIES 5
// multiplier used to calculate number of available blocks from C_SIZE
#define SDSPI_CSD2_C_SIZE_MULT 1024
// macro to add start and transmission bits to 6 bit command
#define SDSPI_CMD(n) (SDSPI_COMMAND_PRELUDE | ((n)&SDSPI_COMMAND_BITS_MASK))

// list of spi commands: long name, command number, response type
#define SDSPI_CMD_LIST(X) \
    X(GO_IDLE_STATE, 0, 1) \
    X(SEND_OP_COND, 1, 1) \
    X(SWITCH_FUNC, 6, 1) \
    X(SEND_IF_COND, 8, 7) \
    X(SEND_CSD, 9, 1) \
    X(SEND_CID, 10, 1) \
    X(STOP_TRANSMISSION, 12, 1) \
    X(SEND_STATUS, 13, 2) \
    X(SET_BLOCKLEN, 16, 1) \
    X(READ_SINGLE_BLOCK, 17, 1) \
    X(READ_MULTIPLE_BLOCK, 18, 1) \
    X(SET_BLOCK_COUNT, 23, 1) \
    X(WRITE_BLOCK, 24, 1) \
    X(WRITE_MULTIPLE_BLOCK, 25, 1) \
    X(PROGRAM_CSD, 27, 1) \
    X(ERASE_WR_BLK_START_ADDR, 32, 1) \
    X(ERASE_WR_BLK_END_ADDR, 33, 1) \
    X(ERASE, 38, 1) \
    X(LOCK_UNLOCK, 42, 1) \
    X(APP_CMD, 55, 1) \
    X(GEN_CMD, 56, 1) \
    X(READ_OCR, 58, 3) \
    X(CRC_ON_OFF, 59, 1)

// list of application commands available after CMD55
#define SDSPI_ACMD_LIST(X) \
    X(SD_STATUS, 13, 2) \
    X(SEND_NUM_WR_BLOCKS, 22, 1) \
    X(SET_WR_BLK_ERASE_COUNT, 23, 1) \
    X(SD_SEND_OP_COND, 41, 1) \
    X(SET_CLR_CARD_DETECT, 42, 1) \
    X(SEND_SCR, 51, 1)

#define SDSPI_CSD1_FIELDS(X) \
    X(CSD_VERSION, 2, 126) \
    X(READ_BL_LEN, 4, 80) \
    X(C_SIZE, 12, 62) \
    X(C_SIZE_MULT, 3, 47)

#define SDSPI_CSD2_FIELDS(X) \
    X(CSD_VERSION, 2, 126) \
    X(READ_BL_LEN, 4, 80) \
    X(C_SIZE, 22, 48)


#define SDSPI_EXPAND_CMD_ENUM(longname, num, rtype) \
    SDSPI_CMD##num = SDSPI_CMD(num), SDSPI_CMD_##longname = SDSPI_CMD(num),
#define SDSPI_EXPAND_ACMD_ENUM(longname, num, rtype) \
    SDSPI_ACMD##num = 0x80 | SDSPI_CMD(num), SDSPI_ACMD_##longname = 0x80 | SDSPI_CMD(num),
#define SDSPI_EXPAND_CMD_RTYPE_ENUM(longname, num, rtype) \
    SDSPI_CMD##num##_RESPONSE = rtype, SDSPI_CMD_##longname##_RESPONSE = rtype,
#define SDSPI_EXPAND_ACMD_RTYPE_ENUM(longname, num, rtype) \
    SDSPI_ACMD##num##_RESPONSE = rtype, SDSPI_ACMD_##longname##_RESPONSE = rtype,
#define SDSPI_EXPAND_CSD1_LENGTH_ENUM(field, length, position) SDSPI_CSD1_##field##_LENGTH = length,
#define SDSPI_EXPAND_CSD2_LENGTH_ENUM(field, length, position) SDSPI_CSD2_##field##_LENGTH = length,
#define SDSPI_EXPAND_CSD1_POS_ENUM(field, length, position) SDSPI_CSD1_##field##_POS = position,
#define SDSPI_EXPAND_CSD2_POS_ENUM(field, length, position) SDSPI_CSD2_##field##_POS = position,


// CMDs and ACMDs supported in SPI mode
typedef enum { SDSPI_CMD_LIST(SDSPI_EXPAND_CMD_ENUM) SDSPI_ACMD_LIST(SDSPI_EXPAND_ACMD_ENUM) } __packed sdspi_cmd_t;

// response types for all CMDs and ACMDs
typedef enum {
    SDSPI_CMD_LIST(SDSPI_EXPAND_CMD_RTYPE_ENUM) SDSPI_ACMD_LIST(SDSPI_EXPAND_ACMD_RTYPE_ENUM)
} sdspi_cmd_response_t;

// bit positions of fields inside csd, not full list
typedef enum {
    SDSPI_CSD1_FIELDS(SDSPI_EXPAND_CSD1_POS_ENUM) SDSPI_CSD2_FIELDS(SDSPI_EXPAND_CSD2_POS_ENUM)
} sdspi_csd_position_t;

// length of fields inside csd, not full list
typedef enum {
    SDSPI_CSD1_FIELDS(SDSPI_EXPAND_CSD1_LENGTH_ENUM) SDSPI_CSD2_FIELDS(SDSPI_EXPAND_CSD2_LENGTH_ENUM)
} sdspi_csd_length_t;

// known response types for commands
typedef enum { SDSPI_R1 = 1, SDSPI_R2, SDSPI_R3, SDSPI_R7 = 7 } sdspi_responses_t;

// generic result for sd functions
typedef enum {
    SDSPI_OK = 0,
    SDSPI_ERROR_GENERIC,
    SDSPI_ERROR_INVALID_ARGS,
    SDSPI_ERROR_RESPONSE_TIMEOUT,
    SDSPI_ERROR_READ,
    SDSPI_ERROR_WRITE,
    SDSPI_ERROR_RESET,
    SDSPI_ERROR_UNKNOWN_CARD,
    SDSPI_ERROR_NO_CARD,
} __packed sdspi_result_t;

// types of cards that need to be considered
typedef enum {
    SDSPI_CARD_MMC_3,
    SDSPI_CARD_SD_1,
    SDSPI_CARD_SD_2,
    SDSPI_CARD_SDSC_2,
    SDSPI_CARD_SDHC_2,
    SDSPI_CARD_INVALID = -1
} __packed sdspi_card_t;

typedef enum {
    SDSPI_DATATOKEN_REGULAR = 0xfe,
    SDSPI_DATATOKEN_MULTI_WRITE = 0xfc,
    SDSPI_DATATOKEN_STOP_MULTI_WRITE = 0xfd
} __packed sdspi_datatoken_t;

typedef enum {
    SDSPI_DATARESPONSE_ACCEPTED = 0b00101,
    SDSPI_DATARESPONSE_CRC_ERROR = 0b01011,
    SDSPI_DATARESPONSE_WRITE_ERROR = 0b01101
} __packed sdspi_dataresponse_t;

typedef enum { SDSPI_CSD_VERSION_1 = 0, SDSPI_CSD_VERSION_2, SDSPI_CSD_VERSION_UNSUPPORTED = -1 } sdspi_csd_version_t;


// R1 response type of sdcards, most common response. all other responses contain this.
typedef union {
    struct {
        uint8_t idle : 1;
        uint8_t erase_reset : 1;
        uint8_t illegal_cmd : 1;
        uint8_t crc_fail : 1;
        uint8_t erase_error : 1;
        uint8_t address_error : 1;
        uint8_t param_error : 1;
        uint8_t : 1;
    } bits;
    uint8_t byte;
} sdspi_r1_t;
static_assert(sizeof(sdspi_r1_t) == 1, "R1 needs to be exactly one byte.");

// R2 response byte. This is the additional byte of R2 response.
typedef union {
    struct {
        uint8_t locked : 1;
        uint8_t lock_unlock_fail : 1;
        uint8_t general_error : 1;
        uint8_t cc_error : 1;
        uint8_t ecc_fail : 1;
        uint8_t write_prot_error : 1;
        uint8_t erase_param_error : 1;
        uint8_t out_of_range : 1;
    } bits;
    uint8_t byte;
} sdspi_r2_byte_t;
static_assert(sizeof(sdspi_r2_byte_t) == 1, "R2 byte needs to be exactly one byte.");

// R2 response type. First byte is R1.
typedef struct {
    sdspi_r1_t r1;
    sdspi_r2_byte_t r2;
} sdspi_r2_t;
static_assert(sizeof(sdspi_r2_t) == 2, "R2 response needs to be exactly two bytes.");

// R3 response type. R1 followed by 4 byte OCR register.
typedef struct {
    sdspi_r1_t r1;
    uint32_t ocr;
} __packed sdspi_r3_t;
static_assert(sizeof(sdspi_r3_t) == 5, "R3 resonse needs to be exactly 5 bytes.");

// R7 is the same as R3?
typedef sdspi_r3_t sdspi_r7_t;

// union containing response types R1 - R3
typedef union {
    sdspi_r1_t r1;
    sdspi_r2_t r2;
    // sdspi_r3_t r3;
    uint8_t raw[sizeof(sdspi_r3_t)];
} __packed sdspi_response_t;
static_assert(sizeof(sdspi_response_t) == 5, "Common response struct needs to be exactly 5 bytes.");

// structure of an SD SPI command
typedef struct {
    sdspi_cmd_t cmd;  // the command to send with start and transmission bits set
    uint32_t arg;  // 4 byte argument
    uint8_t end : 1;  // bit 0, end bit always 1
    uint8_t crc7 : 7;  // bits 7:1
} __packed sdspi_cmd_frame_t;
static_assert(sizeof(sdspi_cmd_frame_t) == 6, "Command frame needs to be exactly 6 bytes.");

// bitmap for error tokens
typedef union {
    struct {
        uint8_t error : 1;
        uint8_t cc_error : 1;
        uint8_t ecc_error : 1;
        uint8_t out_of_range : 1;
        uint8_t locked : 1;
        uint8_t : 3;
    } bits;
    uint8_t byte;
} sdspi_errortoken_t;
static_assert(sizeof(sdspi_errortoken_t) == 1, "Error Token needs to be exactly 1 byte.");

// structure of a block data packet
typedef union {
    struct {
        sdspi_datatoken_t token;
        uint8_t data[SDSPI_BLOCK_SIZE];
        uint16_t crc16;
    } __packed frame;
    uint8_t raw[SDSPI_BLOCK_SIZE + 3];
} sdspi_data_frame_t;
static_assert(sizeof(sdspi_data_frame_t) == SDSPI_BLOCK_SIZE + 3,
              "Dataframe needs to be 3 bytes longer than SDSPI_BLOCK_SIZE.");

// structure of a CSD/CID data packet
typedef struct {
    sdspi_datatoken_t token;
    uint8_t data[SDSPI_CID_CSD_SIZE];
    uint16_t crc16;
} __packed sdspi_cid_csd_frame_t;
static_assert(sizeof(sdspi_cid_csd_frame_t) == SDSPI_CID_CSD_SIZE + 3,
              "CID/CSD dataframe needs to be exactly 19 bytes.");


typedef uint8_t sdspi_csd_t[SDSPI_CID_CSD_SIZE];
typedef uint8_t sdspi_cid_t[SDSPI_CID_CSD_SIZE];


sdspi_result_t sdspi_init(SPI_HandleTypeDef* spi);
sdspi_result_t sdspi_reset();
sdspi_result_t sdspi_send_command(const sdspi_cmd_t cmd, const uint32_t arg, sdspi_response_t* res, bool hold_cs);
sdspi_result_t sdspi_write(const uint32_t block_addr, const uint16_t block_offset, const uint8_t* data,
                           const size_t length_bytes);
sdspi_result_t sdspi_read(const uint32_t block_addr, const uint16_t block_offset, uint8_t* data,
                          const size_t length_bytes);
sdspi_card_t sdspi_get_card();
bool sdspi_card_inserted();
sdspi_result_t sdspi_get_cid(sdspi_cid_t* cid);
sdspi_result_t sdspi_get_csd(sdspi_csd_t* csd);
uint32_t sdspi_get_max_capacity_blocks(sdspi_csd_t* csd);
uint32_t sdspi_capacity_blocks();


/**
 * @brief    Get an R3 response struct from the common response struct.
 * @param    resp Common response struct.
 * @return   R3 response struct. OCR bytes will be byteswapped to be correct in struct.
 */
static inline sdspi_r3_t sdspi_convert_r3(sdspi_response_t resp) {
    // swap bytes of ocr
    *((uint32_t*)(resp.raw + 1)) = __builtin_bswap32(*(uint32_t*)(resp.raw + 1));
    return *(sdspi_r3_t*)resp.raw;
}


#endif  // SDSPI_H