#ifndef EEPROM_H
#define EEPROM_H


#include <stdbool.h>


#define EEPROM_MAGIC 0xca11ab1e  // magic value to designate that eeprom was initialized
#define EEPROM_USE_BANK2_MAGIC 0xf00dbabe  // magic value to designate to try to boot from bank2
#define EEPROM_BASE 0x8080000  // base address of eeprom
#define EEPROM_MAGIC_ADDRESS (EEPROM_BASE)  // address of magic word
#define EEPROM_USE_BANK2_MAGIC_ADDRESS (EEPROM_BASE + EEPROM_BANK_SIZE - 4)
#define EEPROM_DATA_BASE (EEPROM_BASE + 4)  // base address of usable eeprom data
#define EEPROM_BANK_SIZE (3 * 1024)  // [Byte] size of one eeprom bank
#define EEPROM_DATA_SIZE (EEPROM_BANK_SIZE - 8)  // [Byte] availible bytes for data storage without magic word

#define EEPROM_GET_PTR(offset) ((void*)(EEPROM_DATA_BASE + (offset)))  // macro to get a pointer from a relative address inside eeprom data space
#define EEPROM_BANK_B_VAL(var) (*( (typeof((&var)))( ((uintptr_t)(&var)) + EEPROM_BANK_SIZE ) ))
#define EEPROM_BANK_B_PTR(ptr) ( (typeof((ptr)))( ((uintptr_t)(ptr)) + EEPROM_BANK_SIZE ) )

void eeprom_init(const bool force);
__RAM_FUNC void eeprom_copy_bank();
__RAM_FUNC void eeprom_switch_banks();


#endif  // EEPROM_H