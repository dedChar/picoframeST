#ifndef VERSION_H
#define VERSION_H


#include <stdbool.h>
#include <stdint.h>


typedef struct {
    uint8_t major;
    uint8_t minor;
    uint8_t patch;
    bool dev;
    char* type;
    char* commit;
    char* time;
    char* date;
} version_info_t;


const char *const version_get_str();
const char *const version_get_commit();
const char *const version_get_build_config();
const bool version_is_dev();
const version_info_t *const version_get_info();


#endif  // VERSION_H