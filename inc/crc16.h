#ifndef CRC16_H
#define CRC16_H

// based on https://github.com/hazelnusse/crc7
// lookup table generated with http://www.sunshine2k.de/coding/javascript/crc/crc_js.html
// CRC16_CCIT_ZERO

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>


#define CRC16_POLY 0x1021


uint16_t crc16_get(const uint8_t* data, const size_t length);
bool crc16_verify(const uint16_t crc, const uint8_t* data, const size_t length);


#endif  // CRC16_H