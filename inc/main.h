/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define BTN1_Pin GPIO_PIN_13
#define BTN1_GPIO_Port GPIOC
#define VUSB_SENSE_Pin GPIO_PIN_0
#define VUSB_SENSE_GPIO_Port GPIOA
#define PERIPH_EN_Pin GPIO_PIN_3
#define PERIPH_EN_GPIO_Port GPIOA
#define EINK_CS_Pin GPIO_PIN_4
#define EINK_CS_GPIO_Port GPIOA
#define EINK_SCLK_Pin GPIO_PIN_5
#define EINK_SCLK_GPIO_Port GPIOA
#define EINK_MOSI_Pin GPIO_PIN_7
#define EINK_MOSI_GPIO_Port GPIOA
#define EINK_DC_Pin GPIO_PIN_4
#define EINK_DC_GPIO_Port GPIOC
#define EINK_RST_Pin GPIO_PIN_5
#define EINK_RST_GPIO_Port GPIOC
#define EINK_BUSY_Pin GPIO_PIN_0
#define EINK_BUSY_GPIO_Port GPIOB
#define CARD_CS_Pin GPIO_PIN_12
#define CARD_CS_GPIO_Port GPIOB
#define CARD_SCLK_Pin GPIO_PIN_13
#define CARD_SCLK_GPIO_Port GPIOB
#define CARD_MISO_Pin GPIO_PIN_14
#define CARD_MISO_GPIO_Port GPIOB
#define CARD_MOSI_Pin GPIO_PIN_15
#define CARD_MOSI_GPIO_Port GPIOB
#define CARD_CD_Pin GPIO_PIN_6
#define CARD_CD_GPIO_Port GPIOC
#define BTN2_Pin GPIO_PIN_12
#define BTN2_GPIO_Port GPIOC
#define LED1_Pin GPIO_PIN_4
#define LED1_GPIO_Port GPIOB
#define LED2_Pin GPIO_PIN_5
#define LED2_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
